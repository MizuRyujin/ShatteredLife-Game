# _Shattered Life_ - Desenvolvimento de Jogos Digitais II e Linguagens da Programação

O código fonte situa-se [neste repositório público].

## Introdução

_Adventure/Walking Simulator_ onde o jogador interpreta o papel de uma
pessoa que sofre de _Alzheimer's_ a tentar lembrar-se das suas memórias
perdidas viajando pelas suas idades passadas e recentes.

## Mecânicas e respetivos controlos

- Movimento - 'WASD'
- Apanhar e largar - 'E'
- Sistema de inventário -'Tab'
- Manipulação de mapa de jogo com colocação dinâmica de salas - (Dentro 
  do inventário) Clicar e arrastar para mover;

## Instruções / _Walkthrough_

1. Começar por descobrir a peça (nota) inicial.
2. (Abrir inventário) Colocar nota na posição central à direita de modo a conseguir movimentar para a primeira sala objetivo.
3. Na próxima sala, beber uma chávena de café, apanhar nota no chão
   apanhar carro, "apanhar porta", teletransportando-se de volta para a
   sala inicial.
4. Depois, de volta à sala principal, encontrar a disposição certa
   dos_slots_ de modo a conseguir navegar até ao ‘X’, posição central 1
   corredor estreito em cima e em cima desse um corredor em 'L'.
5. Quando na sala ‘X’, terá de apanhar uma peça, das 3 disponíveis e
   largar em cima da pista de forma a esta mudar para uma forma que
   contém uma rampa.
6. Apanhar boneco situado na mesa de cabeceira de forma a tomar a sua
   forma.
7. Quando na forma do boneco, interagir com a pista de forma a viajar na
   mesma até se poder movimentar outra vez.
8. Em cima da estante, viajar por duas rampas até achar um berlinde
   vermelho, pegar neste e largando-o em cima da terceira rampa fazendo com que os blocos desimpeçam a saída da sala.
9. Passar por cima da rampa até à porta roxa, teletransportando-se para
    uma sala com uma nota final.
10. Apanhar a última folha.

## Autoria

### [João Rebelo - a21805230]

- Classe `PickUp.cs` 70%;
- Classe `RaceTrack.cs` (mais as classes necessárias para interagir com esta);
- Implementação de sons com respetivos _triggers_ e scripts;
- Bloquear jogador nas salas;
- Efeito de flutuar;

### [Miguel Fernández - a21803644]

- Movimento e _Head Bob_ de jogador;
- Teletransporte para o prefab `VoidRoom`;
- _Lightmaps_;
- Implementação da viagem da pista;
- Script de Interactable.

### [Rodrigo Pinheiro - 21802488]

- *Shaders*;
- Classe `PickUp` 30% (bug fixes);
- Texto por cima dos objetos que dão para apanhar;
- Sistema de “pausa” do jogo;
- Sistema de Inventário;
- Montagem do puzzle de inventário;
- Implementação de _shaders_;
- Implementação de uma forma de limpar o mapa após um interagível ser
  acedido;
- Sistema de som, `AudioManager`, _Mixer_;

### [Tomás Franco - 21803301]

- Sistema de colocação de salas;
- Combinar a colocação de salas com o sistema de inventário;
- Menu inicial;
- Aperfeiçoar mecânicas de sala implementadas anteriormente;
- Script de Interactable.

## Arquitetura da solução

### Descrição da solução

#### Organização

O programa foi organizado de forma a obedecer ao máximo de princípios 
_SOLID_ e a princípios gerais da POO. Como ferramenta para resolver problemas de implementação utilizámos os seguintes patterns e seguimo-nos pelos seguintes princípios...

#### _Patterns_ usados

- **_Observer Pattern_**, Este pattern é nos essencial pois não queremos o update
várias verificações dos estado do mapa de jogo e do inventário.
- **_Prototype Pattern_**, adicionar os itens ao inventário. Todo o
  projeto foi baseado no conceito de criar _prefabs_ de modo a se poder
  utilizar várias instâncias iguais(com parâmetros costumáveis) do mesmo
  objeto.

#### Princípios

- **_Single Responsability Principle_**: este príncipio é transversal a
  quase todos os _scripts_ do projeto, no entanto, houve momentos em que
  foram criados _scripts_ pequenos com uma tarefa muito específica (mas
  reutilizável) em que este príncipio é ainda mais concreto. Exemplos,
  `Float.cs`, `TriggerSong.cs`.
- **_Dependency Inversion_**: Ao longo do projeto tentámos ao máximo
  minimizar as dependências, daí obedecendo fortemente ao _Single
  Responsability principle_.
- **_Hollywood Principle_**: Este princípio é visível no _script_
  `HouseCtrl.cs` e no sistema de instanciação de salas.

#### Problemas específicos

- **Posicionamento de salas**: No jogo a mecânica principal de colocar no
  inventário o próprio mapa e gerar o mesmo no mundo de jogo trouxe alguns
  problemas maioritariamente em design das classes, assim como a tradução
  de posição no inventário para posição no mundo. Estes problemas foram
  solucionados com o uso de um observer pattern para ligação das
  diferentes funcionalidades, e com uma limitação física na escala das
  salas, sendo apenas possíveis serem 10 por 10.

- **Traduzir espaço mundo para espaço de UI**: Este foi o segundo problema
  mais relatável, sendo que foi resolvido com algumas funcionalidades para
  traduzir transformadas do unity e da implementação de interfaces
  específicas por exemplo `IDragHandler`.

### Diagrama UML de Classes

![UML]

## Referências

- Técnica de _cel-shading_ usada no projeto é baseada na encontrada no
  blog de [Erik Roystan] com algumas modificações para mapas especulares.
  
- _Script_ para associação de _LightMaps_ a _prefabs_ usado neste projeto
  encontra-se neste [repositório], criado por [Ayfel].

[UML]:Images/UML.png
[neste repositório público]:(https://github.com/MizuRyujin/ShatteredLife-Game)
[João Rebelo - a21805230]:(https://github.com/JBernardoRebelo)
[Miguel Fernández - a21803644]:(https://github.com/MizuRyujin)
[Rodrigo Pinheiro - 21802488]:(https://github.com/RodrigoPrinheiro)
[Tomás Franco - 21803301]:(https://github.com/ThomasFranque)
[repositório]:(https://github.com/Ayfel/PrefabLightmapping)
[Ayfel]:(https://github.com/Ayfel)
[Erik Roystan]:https://roystan.net/