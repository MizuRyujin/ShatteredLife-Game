﻿ Shader "Custom/DissolveScroll" {
     Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		[NoScaleOffset] _NoiseTex ("Noise Dissolve Texture", 2D) = "white" {}
		_EdgeColour1 ("Edge colour 1", Color) = (1.0, 1.0, 1.0, 1.0)
		_EdgeColour2 ("Edge colour 2", Color) = (1.0, 1.0, 1.0, 1.0)
		_Level ("Dissolution level", Range (0.0, 1.0)) = 0.1
		_Edges ("Edge width", Range (0.0, 1.0)) = 0.1

		[Header(Distortion)]
		[NoScaleOffset] _FlowTex("Flow texture", 2D) = "white"{}
		_XJump ("X scroll", Range(-0.25, 0.25)) = 0.25
		_YJump ("Y scroll", Range(-0.25, 0.25)) = 0.25
		_Tiling("Tiling repeat", Float) = 1
		_Speed("Animation Speed", Float) = 1
		_Strength("Distortion Strength", Float) = 1
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 100

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
        	Lighting Off
        	ZWrite Off
        	Fog { Mode Off }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex, _NoiseTex, _FlowTex;

			float4 _EdgeColour1;
			float4 _EdgeColour2;
			float4 _Color;
			float _Level;
			float _Edges;
			float4 _MainTex_ST;
			
			fixed _XJump, _YJump;
			fixed _Tiling, _Speed, _Strength;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			// Scroll functions 
			float3 FlowUV (float2 uv, float2 flowVector, float2 jump, float time, bool flowType) 
			{
				float phaseOffset = flowType ? 0.5 : 0;
				float progress = frac(time + phaseOffset);
				float3 uvw;

				uvw.xy = uv - flowVector * progress;
				uvw.xy *= _Tiling;
				uvw.xy += phaseOffset;
				uvw.xy += (time - progress) * jump;
				uvw.z = 1 - abs(1 - 2 * progress);
				
				return uvw;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				// Flow move vector from texture
				float2 flowVector = tex2D(_FlowTex, i.uv).rg * 2 - 1;
				flowVector *= _Strength;
				float2 jump = float2(_XJump, _YJump);
				
				// Scroll color coordinates
				float noise = tex2D(_FlowTex, i.uv).a;
				float time = _Time.y * _Speed + noise;
				float3 sUVA = FlowUV(i.uv, flowVector, jump, time, false);
				float3 sUVB = FlowUV(i.uv, flowVector, jump, time, true);
				
				half4 texA = tex2D(_MainTex, sUVA.xy) * sUVA.z;
				half4 texB = tex2D(_MainTex, sUVB.xy) * sUVB.z;
				
				// sample the noise dissolve texture
				float cutout = tex2D(_NoiseTex, i.uv).r;
				fixed4 c = (texA + texB) * _Color;

				if (cutout < _Level) discard;

				if(cutout < c.a && cutout < _Level + _Edges)
					c =lerp(_EdgeColour1, _EdgeColour2, (cutout-_Level)/_Edges );

				return c;
			}

			ENDCG
		}
	}
 }