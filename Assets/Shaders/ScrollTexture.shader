﻿ Shader "Custom/ScrollTexture" {
     Properties{
     _Color("Color", Color) = (1,1,1,1)
     _MainTex("Texture", 2D) = "white" {}
     [NoScaleOffset] _FlowTex("Flow texture", 2D) = "white"{}
     [NoScaleOffset] _NoiseTex("Dissolve Texture", 2D) = "white"{}
     [Toggle(RIM_TOGGLE)]
     _RimToggle("Toggle Rim Light", float) = 0
     [HDR]_RimColor("Rim Color", Color) = (0.26,0.19,0.16,0.0)
     _RimPower("Rim Power", Range(0.1,2.0)) = 3.0
     _XJump ("X scroll", Range(-0.25, 0.25)) = 0.25
     _YJump ("Y scroll", Range(-0.25, 0.25)) = 0.25
     _Tiling("Tiling repeat", Float) = 1
     _Speed("Animation Speed", Float) = 1
     _Strength("Distortion Strength", Float) = 1
     }

     SubShader{
        Tags{"RenderType" = "Opaque" }
        CGPROGRAM
        #pragma surface surf Lambert
        struct Input {
            float2 uv_MainTex;
            float2 uv_SubTex;
            float3 viewDir;
        };
        sampler2D _MainTex, _FlowTex, _NoiseTex;
        float4 _RimColor;
        float _RimPower;
        fixed4 _Color;
        fixed _XJump;
        fixed _YJump;
        fixed _Tiling, _Speed, _Strength;
    
        float3 FlowUV (float2 uv, float2 flowVector, float2 jump, float time, bool flowType) 
        {
           float phaseOffset = flowType ? 0.5 : 0;
           float progress = frac(time + phaseOffset);
           float3 uvw;

           uvw.xy = uv - flowVector * progress;
           uvw.xy *= _Tiling;
           uvw.xy += phaseOffset;
           uvw.xy += (time - progress) * jump;
           uvw.z = 1 - abs(1 - 2 * progress);
           
           return uvw;
        }

        void surf(Input IN, inout SurfaceOutput o) {
            float2 flowVector = tex2D(_FlowTex, IN.uv_MainTex).rg * 2 - 1;
            flowVector *= _Strength;
            float2 jump = float2(_XJump, _YJump);

            float noise = tex2D(_FlowTex, IN.uv_MainTex).a;
            float time = _Time.y * _Speed + noise;
            float3 sUVA = FlowUV(IN.uv_MainTex, flowVector, jump, time, false);
            float3 sUVB = FlowUV(IN.uv_MainTex, flowVector, jump,time, true);
            
            half4 texA = tex2D(_MainTex, sUVA.xy) * sUVA.z;
            half4 texB = tex2D(_MainTex, sUVB.xy) * sUVB.z;

            fixed4 c = (texA + texB) * _Color;
            o.Albedo = c.rgb;
            o.Albedo += c.rbg;
            // Rim light
            half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
            o.Emission = _RimColor.rgb * pow(rim, _RimPower);
        }
        ENDCG
     }
         Fallback "Diffuse"
 }