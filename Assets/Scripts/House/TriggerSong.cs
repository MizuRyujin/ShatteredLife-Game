﻿/// @file
/// @brief this file plays sounds whenever the player collides
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Detects a collision and plays a song once
/// </summary>
public class TriggerSong : MonoBehaviour
{
    /// <summary>
    /// Instance of AudioCaller - Audio Manager
    /// </summary>
	[SerializeField] private AudioCaller _audioManager = default;

    /// <summary>
    /// Int to check if it has been played or not
    /// </summary>
    private int _count = 0;

    /// <summary>
    /// Detects a trigger enter and plays song if it has not been played before
    /// </summary>
    private void OnTriggerEnter()
    {
        if (_count == 0)
        {
            // Play old man song
            _audioManager.PlayMusic(1);
            _count++;
        }
    }
}
