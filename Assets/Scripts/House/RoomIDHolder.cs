﻿/// @file
/// @brief This file holds the room id
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Room id is given to all independent room prefabs
/// </summary>
public class RoomIDHolder : MonoBehaviour
{
    /// <summary>
    /// Room ID
    /// </summary>
    [SerializeField] private int _roomID = default;
    
    /// <summary>
    /// Room ID
    /// </summary>
    public int RoomID => _roomID;
}
