﻿/// @file
/// @brief This file prevents player from falling indefinitely
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// ResetPlane is the plane that keeps the player from falling in case it 
/// removes a room in which he is standing
/// </summary>
public class ResetPlane : MonoBehaviour
{
    /// <summary>
    /// World spawnpoint where the player will be teleported to
    /// </summary>
    [SerializeField] private Transform _spawnPointTransform = default;

    /// <summary>
    /// Whenever the contacts with the plane collider
    /// </summary>
    /// <param name="other">collision collider</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            ResetPlayerPos(other.transform);
    }

    /// <summary>
    /// Teleports player back
    /// </summary>
    /// <param name="pTransform">player transform</param>
    private void ResetPlayerPos(Transform pTransform)
    {
        // Disable character controller component
        CharacterController pCharCtrlr = 
            pTransform.GetComponent<CharacterController>();
        pCharCtrlr.enabled = false;
        // Put player at spawnpoint
        pTransform.position = _spawnPointTransform.position;
        pCharCtrlr.enabled = true;
    }
}
