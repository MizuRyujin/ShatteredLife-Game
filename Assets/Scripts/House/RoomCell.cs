﻿/// @file
/// @brief This file handles independent room cells info
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Independent roomcell info
/// </summary>
public class RoomCell
{
	// Constants
	/// <summary>
	/// Cell grid X size
	/// </summary>
	private const float MAX_CELL_SIZE_X = 10;

	/// <summary>
	/// Cell grid Z size
	/// </summary>
	private const float MAX_CELL_SIZE_Z = 10;

	// Properties
	/// <summary>
	/// Current room object instance
	/// </summary>
	/// <value></value>
	public GameObject CurrentRoom { get; set; }

	/// <summary>
	/// Distinct room ID
	/// </summary>
	/// <value></value>
    public int RoomID { get; set; }

	/// <summary>
	/// Grid position
	/// </summary>
	private Vector2 _cellPositionInGrid;

	/// <summary>
	/// Grid position
	/// </summary>
	/// <value></value>
	public Vector2 PositionInGrid { get => _cellPositionInGrid; }

	/// <summary>
	/// World position
	/// </summary>
	private Vector3 _cellPositionInWorld;

	/// <summary>
	/// World Position
	/// </summary>
	/// <value></value>
	public Vector3 PositionInWorld { get => _cellPositionInWorld; }

	/// <summary>
	/// Roomcell constructor
	/// </summary>
	/// <param name="posX">X position of cell</param>
	/// <param name="posY">Y position of cell</param>
	public RoomCell(byte posX, byte posY)
	{
		_cellPositionInGrid = new Vector2(posX, posY);
		_cellPositionInWorld = GetPositionInWorld(_cellPositionInGrid);
	}
	
	/// <summary>
	/// Will change the current room if possible.
	/// </summary>
	/// <param name="newRoom">Desired room to place in this cell.</param>
	/// <returns>True if the change was successful and False if not.</returns>
	public bool TryChangeCellRoom(GameObject newRoom)
	{
		// CHANGE LATER, PROBABLY WONT BE NECESSARY
		bool success = true;

		// Check if the room is not the same
		if (newRoom == CurrentRoom) return success;

		return success;
	}

	/// <summary>
	/// Return cell world position
	/// </summary>
	/// <param name="positionInCell">Position on the grid</param>
	/// <returns>World position</returns>
	private Vector3 GetPositionInWorld(Vector3 positionInCell)
	{
		float x = -MAX_CELL_SIZE_X * 2 + (MAX_CELL_SIZE_X * (positionInCell.x));
		float z = -MAX_CELL_SIZE_Z * 2 + (MAX_CELL_SIZE_Z * (positionInCell.y));

		return new Vector3(x, 0f,z);
	}
}
