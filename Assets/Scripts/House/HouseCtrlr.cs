﻿/// @file
/// @brief This file handles room instantiation, placing and grid information
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// HouseCtrlr holds the house grid used to place rooms
/// </summary>
public class HouseCtrlr : MonoBehaviour
{
    /// <summary>
    /// World cells collection
    /// </summary>
    private List<RoomCell> _roomCells;

    /// <summary>
    /// Know if a grid location is occupied
    /// </summary>
    /// <param name="cellLocation">Location in cell</param>
    /// <returns>True if occupied</returns>
    public bool IsCellOccupied(Vector2 cellLocation) =>
        FindCellScriptInGridLocation(cellLocation).CurrentRoom != null;

    /// <summary>
    /// Initializes collection
    /// </summary>
    private void Awake()
    {
        // Define the Cells
        _roomCells = new List<RoomCell>(25) { };

        int index = 0;
        for (int i = 0; i < 25; i++)
        {
            _roomCells.Add(new RoomCell((byte)(i % 5), (byte)index));
            if (i % 5 == 4)
                index++;
        }
    }

    /// <summary>
    /// Change current room in a cell
    /// </summary>
    /// <param name="newRoom">New room to instantiate</param>
    /// <param name="cellLocation">Position in the grid</param>
    /// <param name="rotation">Room rotation</param>
    public void ChangeRoom(GameObject newRoom, Vector2 cellLocation, Quaternion rotation = default)
    {
        RoomCell cellOnGridScript = FindCellScriptInGridLocation(cellLocation);
        ChangeRoom(newRoom, cellOnGridScript, rotation);

    }

    // Change a room in a cell
    /// <summary>
    /// Change current room in a cell
    /// </summary>
    /// <param name="newRoom">Room to instantiate</param>
    /// <param name="cellScript">Current cell script</param>
    /// <param name="rotation">Cell rotation</param>
    private void ChangeRoom(GameObject newRoom, RoomCell cellScript, Quaternion rotation = default)
    {
        if (cellScript != null)
        {
            GameObject currentRoom = cellScript.CurrentRoom;

            if (cellScript.TryChangeCellRoom(newRoom))
            {
                RemoveRoomInCell(cellScript);
                cellScript.CurrentRoom =
                    Instantiate(
                        newRoom,
                        cellScript.PositionInWorld,
                        rotation,
                        transform);
                cellScript.RoomID = newRoom.GetComponent<RoomIDHolder>().RoomID;
            }
        }
    }

    /// <summary>
    /// Clear all cells
    /// </summary>
    public void ClearHouseCells()
    {
        foreach (RoomCell r in _roomCells)
            RemoveRoomInCell(r);

        // Needs to update UI
        OnClear();
    }

    /// <summary>
    /// Find a cell script on on a cell position
    /// </summary>
    /// <param name="cellLocation">Grid location of cell</param>
    /// <returns>The cell script on grid</returns>
    public RoomCell FindCellScriptInGridLocation(Vector2 cellLocation)
    {
        RoomCell cellOnGridScript = null;

        foreach (RoomCell r in _roomCells)
            if (cellLocation == r.PositionInGrid)
            {
                cellOnGridScript = r;
                break;
            }
        return cellOnGridScript;
    }

    /// <summary>
    /// Remove a room in the given cell position
    /// </summary>
    /// <param name="cellLocation">Cell position in grid</param>
    public void RemoveRoomInCell(Vector2 cellLocation)
    {
        RoomCell cellScript = FindCellScriptInGridLocation(cellLocation);

        RemoveRoomInCell(cellScript);
    }

    /// <summary>
    /// Remove a room in the given cell position
    /// </summary>
    /// <param name="cellScript">Cell to remove</param>
    public void RemoveRoomInCell(RoomCell cellScript)
    {
        if (cellScript.CurrentRoom != null) Destroy(cellScript.CurrentRoom);
    }

    // Invokes clear action
    private void OnClear()
    {
        Clear?.Invoke();
    }

    // Clear action
    public event Action Clear;
}