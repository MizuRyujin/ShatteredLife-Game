﻿/// @file
/// @brief This file contains specific behaviours for the player
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Player script for specific functions
/// </summary>
public class Player : AbstractPlayer
{
    /// <summary>
    /// Reference to inventory vCam
    /// </summary>
    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera _invCamera = default;

    /// <summary>
    /// Reference for the script that controls inventory
    /// </summary>
    private InventoryController _inventoryController = default;

    /// <summary>
    /// Reference to Pause script
    /// </summary>
    private Pause _pauseScript = default;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        _inventoryController = GetComponentInChildren<InventoryController>();
        _pauseScript = (Pause)GetComponent("Pause");
    }

    /// <summary>
    /// Methods to be called when inventory is enable
    /// </summary>
    private void OnEnable()
    {
        _inventoryController.InventoryEnable += ToggleCamera;
        _inventoryController.InventoryEnable += ToggleMovement;
        _inventoryController.InventoryEnable += LookToInventory;

        _inventoryController.InventoryDisable += ToggleMovement;
        _inventoryController.InventoryDisable += ToggleCamera;
        _inventoryController.InventoryDisable += NotLookToInventory;
    }

    /// <summary>
    /// Override to base class Update method
    /// </summary>
    protected override void Update()
    {
        base.Update();

        if (_actionManUse)
        {
            _pauseScript.enabled = false;
        }
        else
        {
            _pauseScript.enabled = true;
        }
    }

    /// <summary>
    /// Methods to be called when inventory is disable
    /// </summary>
    private void OnDisable()
    {
        _inventoryController.InventoryEnable -= ToggleCamera;
        _inventoryController.InventoryEnable -= ToggleMovement;
        _inventoryController.InventoryEnable -= LookToInventory;

        _inventoryController.InventoryDisable -= ToggleCamera;
        _inventoryController.InventoryDisable -= NotLookToInventory;
        _inventoryController.InventoryDisable -= ToggleMovement;

    }

    /// <summary>
    /// Method to enable inventory VCam
    /// </summary>
    private void LookToInventory()
    {
        if (!_actionManUse)
        {
        _invCamera.enabled = true;
        _cameraMachine.enabled = false;
        }
    }

    /// <summary>
    /// Method to disable inventory VCam
    /// </summary>
    private void NotLookToInventory()
    {
        if (!_actionManUse)
        {
        _invCamera.enabled = false;
        _cameraMachine.enabled = true;
        }
    }
}
