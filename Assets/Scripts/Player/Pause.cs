﻿/// @file
/// @brief This file contains the Pause script witch
/// controls the pause blinking and reset mechanic of the game.
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using System.Collections;
using UnityEngine;

/// <summary>
/// Pause script witch controls the pause blinking and reset mechanic of the game.
/// </summary>
public class Pause : MonoBehaviour
{
    /// <summary>
    /// Time to pause dictates the amount of time the player has to be still
    /// for the game to reset his location.
    /// </summary>
    [SerializeField] private float _timeToPause = 1;

    /// <summary>
    /// Time wait between animations.
    /// </summary>
    [SerializeField] private float _waitTimeBetweenAnimation = 1;

    /// <summary>
    /// Character controller of the player.
    /// </summary>
    private CharacterController _ca = default;

    /// <summary>
    /// Inventory controller of the player.
    /// </summary>
    private InventoryController _ic = default;

    /// <summary>
    /// Current time tracking variable.
    /// </summary>
    private float _currentTime = default;

    /// <summary>
    /// Pause effect script to trigger the animations.
    /// </summary>
    private PauseEffect _pauseTransition = default;
    
    /// <summary>
    /// Wait for seconds instance to use in the coroutine
    /// </summary>
    private WaitForSeconds _waitSomeTime = default;

    /// <summary>
    /// Current coroutine being played by the pause animation.
    /// </summary>
    private Coroutine _teleportRoutine = default;

    /// <summary>
    /// Control variable to check if the animation has  been played.
    /// </summary>
    private bool _wasReset = default;

    /// <summary>
    /// If the inventory is open.
    /// </summary>
    private bool _inventoryOpened = default;

    private void OnEnable()
    {
        _ic.InventoryEnable += SetInnactive;
        _ic.InventoryDisable += SetActive;
    }

    private void OnDisable()
    {

        _ic.InventoryEnable -= SetInnactive;
        _ic.InventoryDisable -= SetActive;
    }

    private void Awake()
    {
        _ic = GetComponentInChildren<InventoryController>();   
    }

    private void Start()
    {
        _ca = GetComponent<CharacterController>();
        _pauseTransition = GetComponentInChildren<PauseEffect>();
        _waitSomeTime = new WaitForSeconds(_waitTimeBetweenAnimation);
        _currentTime = 0;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_inventoryOpened) InputCheck();

        if (_currentTime >= _timeToPause)
        {
            _wasReset = true;
            _currentTime = 0;
            _teleportRoutine = StartCoroutine(ResetPosition());
        }
    }

    /// <summary>
    /// Check if an input was done by the player.
    /// </summary>
    private void InputCheck()
    {
        if (Input.anyKey)
        {
            _wasReset = false;
            if (_teleportRoutine != null)
                StopCoroutine(_teleportRoutine);
        }

        if (!Input.anyKey && !_wasReset)
        {
            _currentTime += Time.deltaTime;
        }
        else
            _currentTime = 0;
    }

    /// <summary>
    /// Sets the pause effect innactive.
    /// </summary>
    private void SetInnactive()
    {
        _inventoryOpened = true;
    }

    /// <summary>
    /// Sets the pause effect active.
    /// </summary>
    private void SetActive()
    {
        _inventoryOpened = false;
    }

    /// <summary>
    /// Coroutine that plays the animation and resets the player's position.
    /// </summary>
    /// <returns> Waits for the time between animations</returns>
    private IEnumerator ResetPosition()
    {
        _pauseTransition.Blink();
        yield return _waitSomeTime;
        _pauseTransition.Blink();
        yield return _waitSomeTime;
        _pauseTransition.CloseEyes();
        yield return _waitSomeTime;
        _ca.enabled = false;
        transform.position = Vector3.zero;
        _ca.enabled = true;
        yield break;
    }
}
