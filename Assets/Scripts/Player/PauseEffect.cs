﻿/// @file
/// @brief This file contains the blink pause effect control script for the player
/// idle animation.
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Pause effect controls the animator triggers for the blinking canvas effect
/// when the player is idle for too long.
/// </summary>
public class PauseEffect : MonoBehaviour
{
    /// <summary>
    /// Player's animator
    /// </summary>
    private Animator _animator;

    /// <summary>
    /// Blink speed, time between blink animation.
    /// </summary>
    [SerializeField] private float _blinkSpeed = default;

    /// <summary>
    /// Close speed of the effect when the player character closes his eyes.
    /// </summary>
    [SerializeField] private float _closeSpeed = default;

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        _animator = GetComponent<Animator>();    
    }

    /// <summary>
    /// Blink method changes the animator state to blink with a given speed.
    /// </summary>
    public void Blink()
    {
        _animator.SetFloat("open_close_Speed", _blinkSpeed);
        _animator.SetTrigger("blink");
    }

    /// <summary>
    /// Close eyes method changes the animator state to close the player's eyes
    /// with the given speed parameter.
    /// </summary>
    public void CloseEyes()
    {
        _animator.SetFloat("open_close_Speed", _closeSpeed);
        _animator.SetTrigger("close");
    }
}
