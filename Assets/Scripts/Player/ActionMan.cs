﻿/// @file
/// @brief This file contains specific behaviours for the action man
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Class responsible for Action Man specific behaviours
/// </summary>
public class ActionMan : AbstractPlayer
{
    /// <summary>
    /// Variable that contains references for the meshes that compose Action Man
    /// game object
    /// </summary>
    [SerializeField]
    private Renderer[] _aMeshes = default;

    /// <summary>
    /// Reference for the Pick Up script
    /// </summary>
    [SerializeField]
    private PickUp _pickUpScript = default;

    /// <summary>
    /// Method to handle the race track interaction
    /// </summary>
    public void RaceTrackInteraction()
    {
        _cameraMachine.enabled = !_cameraMachine.enabled;
        ToggleCamera();
        ToggleMovement();
    }

    /// <summary>
    /// Method to disable game object meshes when Action Man is riding the track
    /// </summary>
    public void DisableActionMesh()
    {
        for (int i = 0; i < _aMeshes.Length; i++)
        {
            _aMeshes[i].enabled = false;
        }
        _pickUpScript.ClearFloatingText();
        _pickUpScript.enabled = false;
    }

    /// <summary>
    /// Method to enable game object meshes when Action Man is riding the track
    /// </summary>
    public void EnableActionMesh()
    {
        for (int i = 0; i < _aMeshes.Length; i++)
        {
            _aMeshes[i].enabled = true;
        }
        _pickUpScript.enabled = true;
    }
}
