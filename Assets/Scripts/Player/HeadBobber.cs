﻿/// @file
/// @brief 
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

public class HeadBobber : MonoBehaviour
{
    [SerializeField]
    private float timer = default;
    [SerializeField]
    private float _bobbingSpeed = default;
    [SerializeField]
    private float _bobbingAmount = default;
    [SerializeField]
    private float _midpoint = default;

    void Update()
    {
        float waveslice = 0.0f;
        float horizontal = Input.GetAxis("Forward");
        float vertical = Input.GetAxis("Strafe");

        Vector3 camPosition = transform.localPosition;

        if (Mathf.Abs(horizontal) == 0 && Mathf.Abs(vertical) == 0)
        {
            timer = 0.0f;
        }
        else
        {
            waveslice = Mathf.Sin(timer);

            timer += _bobbingSpeed;

            if (timer > Mathf.PI * 2)
            {
                timer -= (Mathf.PI * 2);
            }
        }

        if (waveslice != 0)
        {
            float translateChange = waveslice * _bobbingAmount;

            float totalAxes = Mathf.Clamp(
                horizontal + vertical, -1.0f, 1.0f);

            translateChange = totalAxes * translateChange;

            camPosition.y = _midpoint + translateChange;
            camPosition.x = translateChange;

            if (waveslice < -0.3f)
            {
                translateChange = waveslice * _bobbingAmount;

                totalAxes = Mathf.Clamp(
                    horizontal + vertical, -1.0f, 1.0f);

                translateChange = totalAxes * translateChange;

                camPosition.y = _midpoint - translateChange;
                camPosition.x = translateChange;
            }
        }
        else
        {
            camPosition.y = _midpoint;
            camPosition.x = 0;
        }

        transform.localPosition = camPosition;
    }
}


