﻿/// @file
/// @brief This file is used to make a cheatcode for the game so
/// we can skip a whole sequence.
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020

using UnityEngine;

/// <summary>
/// Class waits for user input of the key 'P' and adds to the inventory
/// the two paper pieces needed to complete de vertical slice.
/// </summary>
public class InventoryCheat : MonoBehaviour
{
    /// <summary>
    /// Papers to give to the player
    /// </summary>
    [SerializeField] private Paper[] _papers = default;

    /// <summary>
    /// Were the papers given, so we avoid giving infinite papers.
    /// </summary>
    private bool _papersGiven = false;

    /// <summary>
    /// Inventory controller so we add the item.
    /// </summary>
    private InventoryController _controller;

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<InventoryController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_papersGiven)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                foreach(Paper p in _papers)
                    _controller.AddItem(p);

                _papersGiven = true;
            }
        }
    }
}
