﻿/// @file
/// @brief This file contains the Controller that connects UI and mechanic elements
/// for the player inventory.
/// 
/// @author João Rebelo, Miguel Fernandéz, Rodrigo Pinheiro e Tomás Franco.
/// @date 2020
using UnityEngine;
using System;

/// <summary>
/// Inventory controller uses the inventory slots and the book game object to
/// add and remove items to the UI and to be available to the player. This is
/// made primarily by using events.
/// </summary>
public class InventoryController : MonoBehaviour
{
    /// <summary>
    /// Key to open the inventory
    /// </summary>
    [SerializeField] private KeyCode _keyOpenInventory = default;
    /// <summary>
    /// Transform for the parent of the inventory slots
    /// </summary>
    [SerializeField] private Transform _inventorySlots = default;
    /// <summary>
    /// A prefab for the Object that holds the items in the inventory grid.
    /// </summary>
    [SerializeField] private Draggable _itemHolder = default;
    /// <summary>
    /// The in game asset game object for the inventory book
    /// </summary>
    [SerializeField] private GameObject _book = default;

    /// <summary>
    /// Audioclip for inventory enable/disable
    /// </summary>
    [SerializeField] private AudioClip _useSound = default;

    /// <summary>
    /// Instance of AudioCaller - Audio Manager
    /// </summary>
	[SerializeField] private AudioCaller _audioManager = default;

    /// <summary>
    /// Animator for the inventory
    /// </summary>
    private Animator _anim = default;

    /// <summary>
    /// Boolean to control if the inventory is open or not.
    /// </summary>
    private bool _isActive = default;

    /// <summary>
    /// Reference to the player script.
    /// </summary>
    private Player _pScript = default;

    /// <summary>
    /// Sets animator variables and Gets components
    /// </summary>
    private void Start()
    {
        Cursor.visible = false;
        _anim = GetComponent<Animator>();
        _isActive = false;
        _anim.SetBool("isActive", _isActive);
        _pScript = GetComponentInParent<Player>();
    }

    /// <summary>
    /// Adds a new item to the player inventory by instantiating the new item
    /// into the player inventory slots.
    /// </summary>
    /// <param name="newItem"> The new item to add of type Paper</param>
    public void AddItem(Paper newItem)
    {
        Paper inventoryPaper = Instantiate(newItem, newItem.transform);
        Destroy(newItem.gameObject);

        foreach (Transform t in _inventorySlots)
        {
            if (t.childCount == 0)
            {
                Transform holder = Instantiate(_itemHolder, t.position, t.rotation, t)
                    .transform;

                inventoryPaper.transform.SetParent(holder);
                inventoryPaper.transform.localPosition = new Vector3(0, 0, -1.5f);
                inventoryPaper.transform.localRotation = Quaternion.Euler(90, 0, 180);
                inventoryPaper.transform.localScale = new Vector3(.1f, .1f, .1f);
                break;
            }
        }
    }

    /// <summary>
    /// Checks key press to open/close inventory.
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(_keyOpenInventory))
        {
            if (!_pScript._actionManUse)
            {
                _isActive = !_isActive;

                // Play Sound
                _audioManager.PlaySound(_useSound, gameObject);

                // Decides which events to call if the inventory is opening or closing
                _anim.SetBool("isActive", _isActive);
                if (_isActive)
                {
                    _book.SetActive(true);
                    OnInventoryEnable();
                }
                else
                {
                    OnInventoryDisable();
                }
            }

        }
    }

    /// <summary>
    /// Method that invokes the open event.
    /// </summary>
    private void OnInventoryEnable()
    {
        Cursor.visible = true;
        InventoryEnable?.Invoke();
    }

    /// <summary>
    /// Method that invokes the close event.
    /// </summary>
    private void OnInventoryDisable()
    {
        Cursor.visible = false;
        InventoryDisable?.Invoke();
    }

    /// <summary>
    /// Method to disable book 3D asset, called in the CloseAnim event.
    /// </summary>
    public void DisableBook()
    {
        _book.SetActive(false);
    }

    // Event for everything inventory needs to do when activating/deactivating
    /// <summary>
    /// Inventory open event.
    /// </summary>
    public event Action InventoryEnable;

    /// <summary>
    /// Inventory close event.
    /// </summary>
    public event Action InventoryDisable;
}
