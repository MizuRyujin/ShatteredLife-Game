﻿/// @file
/// @brief File contains the Map inventory class that contains the control for
/// the map.
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;
using System;

/// <summary>
/// Has the responsibility of changing the state of the map and slots game objects.
/// </summary>
public class MapInventory : MonoBehaviour
{
    /// <summary>
    /// Parent transform for the slot group that contains the map slots.
    /// </summary>
    [SerializeField] private Transform _slotsPanel = null;

    /// <summary>
    /// House controller to update the in game world after closing the inventory.
    /// </summary>
    private HouseCtrlr _houseCtrl = null;
    
    /// <summary>
    /// Inventory reference to control what happens when it closes and opens
    /// through events.
    /// </summary>
    private InventoryController _ic;
    
    /// <summary>
    /// Main data structure used to store the map state.
    /// </summary>
    private Paper[,] _map;

    /// <summary>
    /// Temporary position to use along the script
    /// </summary>
    private Vector2Int _tempPosition;

    private void Awake()
    {
        _ic = GetComponent<InventoryController>();
        _houseCtrl = GameObject.FindWithTag("HouseControl").
            GetComponent<HouseCtrlr>();
    }

    private void Start()
    {
        _map = new Paper[5,5];

    }

    /// <summary>
    /// Updates the map inventory with any placed papers in the slots.
    /// </summary>
    private void UpdateMapInventory()
    {
        for (int i = 0; i < _slotsPanel.childCount; i++)
        {
            int x = i % 5;
            int y = i / 5;

            Paper item = _slotsPanel.GetChild(i).GetComponentInChildren<Paper>();

			_tempPosition.Set(x, y);

            _map[x, y] = item;

			if (_map[x, y] != null)
			{
                _map[x, y].Position = _tempPosition;
			}
        }
    }

    /// <summary>
    /// Clears the map inventory removing it from the game object.
    /// </summary>
    private void ClearMapInventory()
    {
        for (int i = 0; i < _slotsPanel.childCount; i++)
        {
            int x = i % 5;
            int y = i / 5;

            if (_slotsPanel.GetChild(i).childCount > 0)
            {
                Destroy(_slotsPanel.GetChild(i).GetChild(0).gameObject);
            }
        }
    }

    /// <summary>
    /// Updates the map in the game using the HouseController and the
    /// game objects placed in the inventory.
    /// </summary>
    private void UpdateMap()
    {
        for (int i = 0; i < _map.GetLength(0); i++)
        {
            for (int j = 0; j < _map.GetLength(1); j++)
			{
				_tempPosition.Set(i,j);

				if (_map[i, j] != null)
				{
                    RoomIDHolder mapID = _map[i,j].RoomPrefab.GetComponent<RoomIDHolder>();
					if (!_houseCtrl.IsCellOccupied(_tempPosition) || 
						_houseCtrl.FindCellScriptInGridLocation(_tempPosition).
                        RoomID != mapID.RoomID)
					{
						Vector2 cellPosition = _map[i, j].Position;
						GameObject roomPrefab = _map[i, j].RoomPrefab;
						// Rotation to be revised
						Quaternion rotation = Quaternion.identity;

						// Update room on world
						_houseCtrl.ChangeRoom(
							roomPrefab, 
							cellPosition, 
							rotation);
					}
				}
				// Check if the room is occupied and remove it
				else if (_houseCtrl.IsCellOccupied(_tempPosition))
					_houseCtrl.RemoveRoomInCell(_tempPosition);
			}
        }
    }

    /// <summary>
    /// Resets the inventory, clears the map and adds the removed items into
    /// the inventory.
    /// </summary>
    private void ResetInventory()
    {
        foreach(Paper p in _map)
        {
            if (p != null)
                _ic.AddItem(p);
        }

        Array.Clear(_map, 0, _map.Length);
        ClearMapInventory();
        UpdateMap();
    }

    // Update map
    private void OnEnable()
    {
        _houseCtrl.Clear += ResetInventory;
        _ic.InventoryEnable += UpdateMapInventory;
        _ic.InventoryDisable += UpdateMapInventory;
		_ic.InventoryDisable += UpdateMap;
	}

	private void OnDisable()
    {
        _houseCtrl.Clear -= ResetInventory;
        _ic.InventoryEnable -= UpdateMapInventory;
        _ic.InventoryDisable -= UpdateMapInventory;
		_ic.InventoryDisable -= UpdateMap;
	}
}
