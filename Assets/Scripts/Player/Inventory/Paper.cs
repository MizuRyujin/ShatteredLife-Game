﻿/// @file
/// @brief File contains a Paper data container.
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Paper holds the data for a room, it's prefab and current position.
/// </summary>
public class Paper : MonoBehaviour
{
	/// <summary>
	/// Room prefab to instantiate from this paper.
	/// </summary>
	[SerializeField] private GameObject _roomPrefab = null;

	/// <summary>
	/// Read only property to get the paper's room.
	/// </summary>
	public GameObject RoomPrefab => _roomPrefab;

	/// <summary>
	/// Current position of the paper in map.
	/// </summary>
	/// <value> Vector2 with the current position.</value>
    public Vector2Int Position { get; set; }
}
