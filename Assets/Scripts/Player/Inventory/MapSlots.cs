﻿/// @file
/// @brief This file contains the MapSlots script which controls the current 
/// draggables placed in the map.
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020

using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Map slots serve as containers for items as a child object.
/// </summary>
public class MapSlots : MonoBehaviour, IDropHandler
{
    /// <summary>
    /// Current item in the map slot.
    /// </summary>
    /// <value> Current child Game Object of the slot</value>
    public GameObject Item
    {
        get
        {
            if (transform.childCount > 0)
                return transform.GetChild(0).gameObject;

            return null;
        }
    }

    /// <summary>
    /// OnDrop event provided by the unity engine UI elements.
    /// </summary>
    /// <param name="eventData"> Event data provided by camera raycasts</param>
    public void OnDrop(PointerEventData eventData)
    {
        if (!Item)
        {
            eventData.pointerDrag.transform.SetParent(transform);

            Item.transform.position = transform.position;
            Item.transform.rotation = transform.rotation;
            Item.transform.localScale = transform.localScale;
        }
    }
}
