﻿/// @file
/// @brief 
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Draggable objects can be dragged around as if it was a UI element.
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class Draggable : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    /// <summary>
    /// Transform in canvas space.
    /// </summary>
    private RectTransform _canvasRect;

    /// <summary>
    /// Main camera for the interaction ray.
    /// </summary>
    private Camera _mainCam;

    /// <summary>
    /// Start position temporary variable.
    /// </summary>
    private Vector3 _startPosition;

    /// <summary>
    /// Parent transform of the draggable when the user first starts a drag.
    /// </summary>
    private Transform _startParent;

    /// <summary>
    /// Current Canvas group.
    /// </summary>
    private CanvasGroup _group;

    // Debug Tools
    private Vector3 ray;
    private Vector3 rayDir;

    /// <summary>
    /// Current gameobject of the item being dragged.
    /// </summary>
    /// <value></value>
    public static GameObject ItemBeingDragged { get; private set; }

    /// <summary>
    /// Called at the start of an object  life time.
    /// </summary>
    private void Start()
    {
        _mainCam = GameObject.FindGameObjectWithTag("MainCamera")
            .GetComponent<Camera>();
        
        _canvasRect = GetComponentInParent<Canvas>().transform as RectTransform;
        _group = GetComponent<CanvasGroup>();

    }

    /// <summary>
    /// Called when an object begins a drag, given by the IBeginDragHandler
    /// interface.
    /// </summary>
    /// <param name="eventData"> Data of the ray hit.</param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        // Debug ray information
        ray = eventData.pointerCurrentRaycast.worldPosition;
        rayDir = eventData.pointerCurrentRaycast.worldNormal;

        _group.blocksRaycasts = false;
        ItemBeingDragged = gameObject;
        _startPosition = transform.position;
        _startParent = transform.parent;
    }

    /// <summary>
    /// Called when an object is being dragged. Given by the IDragHandler.
    /// </summary>
    /// <param name="eventData"> Data of the ray hit.</param>
    public void OnDrag(PointerEventData eventData)
    {
        ray = eventData.pointerCurrentRaycast.worldPosition;
        rayDir = eventData.pointerCurrentRaycast.worldNormal;

        // Get mouse position in screen
        Vector2 screenPoint = Input.mousePosition;
        Vector3 position;

        // Translate it to canvas position
        RectTransformUtility.ScreenPointToWorldPointInRectangle(_canvasRect, screenPoint, _mainCam, out position);
        transform.position = position;
    }

    /// <summary>
    /// Called when an object is released from a drag.
    /// </summary>
    /// <param name="eventData"> Data of the ray hit.</param>
    public void OnEndDrag(PointerEventData eventData)
    {
        // Debug ray information
        ray = eventData.pointerCurrentRaycast.worldPosition;
        rayDir = eventData.pointerCurrentRaycast.worldNormal;


        _group.blocksRaycasts = true;

        ItemBeingDragged = null;
        if (transform.parent == _startParent)
        {
            transform.SetParent(_startParent);
            transform.position = _startPosition;
        }
        //transform.rotation = _startParent.rotation;
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(ray, rayDir);
    }
}
