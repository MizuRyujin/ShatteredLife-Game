﻿/// @file
/// @brief File contains PuzzleObjective scriptable object. Designer tool
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

[CreateAssetMenu(fileName = "Puzzle Objective", menuName = "ShatteredLife/Puzzle", order = 1)]
/// <summary>
/// Currently unused. PuzzleObjective contains an objective and the room as the
/// objective, so a designer can create different puzzles by changing the location
/// and room.
/// </summary>
public class PuzzleObjective : ScriptableObject
{
    /// <summary>
    /// Objective location
    /// </summary>
    [SerializeField] Vector2Int _objective;

    /// <summary>
    /// Objective room.
    /// </summary>
    [SerializeField] GameObject _room;

    /// <summary>
    /// Read only property to get the current objective location.
    /// </summary>
    public Vector2Int ObjectivePosition => _objective;

    /// <summary>
    /// Read only property to get the objective room.
    /// </summary>
    public GameObject Room => _room;
}
