﻿/// @file
/// @brief This file contains specific behaviours for the player
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// General class for player movement and actions
/// </summary>
public abstract class AbstractPlayer : MonoBehaviour
{
    #region Class Constants
    /// <summary>
    /// Max value for the camera to look down
    /// </summary>
    protected const float MIN_HEAD_LOOK_ROTATION = 300.0f;

    /// <summary>
    /// Max value for the looking up
    /// </summary>
    protected const float MAX_HEAD_LOOK_ROTATION = 60.0f;

    /// <summary>
    /// Max value for camera rotation speed
    /// </summary>
    protected const float MAX_ANG_VELOCITY_FACTOR = 1.2f;

    /// <summary>
    /// Max value for positive movement values
    /// </summary>
    protected const float MAX_POSITIVE_MOVEMENT_VELOCITY = 4f;

    /// <summary>
    /// Max value for negative movement values
    /// </summary>
    protected const float MAX_NEGATIVE_MOVEMENT_VELOCITY = -4f;
    #endregion

    // Class variables

    /// <summary>
    /// Variable for player move speed
    /// </summary>
    [SerializeField] protected float _moveSpeed = default;

    /// <summary>
    /// Variable for player movement acceleration
    /// </summary>
    [SerializeField] protected float _acceleration = default;

    /// <summary>
    /// Variable for gravity factor
    /// </summary>
	[SerializeField] protected float _gravityFactor = 1;

    /// <summary>
    /// Reference for the Virtual Camera
    /// </summary>
    protected Cinemachine.CinemachineVirtualCamera _cameraMachine;

    /// <summary>
    /// Reference to the Character Controller gameObject component
    /// </summary>
    protected CharacterController _controller;

    /// <summary>
    /// Movement direction
    /// </summary>
    protected Vector3 _direction;

    /// <summary>
    /// Actual movement (direction and speed)
    /// </summary>
    protected Vector3 _movement;

    /// <summary>
    /// Variable to check if camera is active
    /// </summary>
    [HideInInspector]
    public bool _cameraActive;

    /// <summary>
    /// Variable to check if player can move
    /// </summary>
    [HideInInspector]
    public bool _canMove;

    /// <summary>
    /// Check if Action Man is being used
    /// </summary>
    [HideInInspector]
    public bool _actionManUse;

    /// <summary>
    /// Walking sound to be played
    /// </summary>
    [SerializeField] private AudioClip _step;

    /// <summary>
    /// Instance of AudioCaller - Audio Manager
    /// </summary>
	[SerializeField] private AudioCaller _audioManager;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    protected virtual void Awake()
    {
        _controller = GetComponent<CharacterController>();
        _cameraMachine = GetComponentInChildren
            <Cinemachine.CinemachineVirtualCamera>();
        _direction = Vector3.zero;
        _movement = Vector3.zero;
    }

    /// <summary>
    /// Start is called before the first frame
    /// </summary>
    protected virtual void Start()
    {
        _cameraActive = _canMove = _cameraMachine.isActiveAndEnabled;
    }

    #region Update_Region

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    protected virtual void Update()
    {
        PlayerRotation();
        HeadRotation();
    }

    /// <summary>
    /// Method to rotate player model with camera rotation
    /// </summary>
    private void PlayerRotation()
    {
        if (!_cameraActive) return;
        float pRotation = Input.GetAxis("Mouse X") * MAX_ANG_VELOCITY_FACTOR;

        transform.Rotate(0.0f, pRotation, 0.0f);
    }

    /// <summary>
    /// Method to rotate camera with mouse input
    /// </summary>
    private void HeadRotation()
    {
        if (!_cameraActive || !_canMove) return;

        Vector3 camRotation = _cameraMachine.transform.localEulerAngles;

        camRotation.x -= Input.GetAxis("Mouse Y") * MAX_ANG_VELOCITY_FACTOR;

        if (camRotation.x > 180.0f)
        {
            camRotation.x = Mathf.Max(camRotation.x, MIN_HEAD_LOOK_ROTATION);
        }
        else
        {
            camRotation.x = Mathf.Min(camRotation.x, MAX_HEAD_LOOK_ROTATION);
        }

        _cameraMachine.transform.localEulerAngles = camRotation;
    }
    #endregion

    #region FixedUpdate_Region
    
    /// <summary>
    /// This function is called every fixed framerate frame, if the 
    /// MonoBehaviour is enabled.
    /// </summary>
    protected virtual void FixedUpdate()
    {
        PlayerVelocity();
        PlayerMovement();
    }

    /// <summary>
    /// Method to calculate player velocity with controller inputs
    /// </summary>
    private void PlayerVelocity()
    {
        // Acceleration section
        _direction.z = Input.GetAxis("Forward") * _acceleration;

        _direction.z *= _acceleration;

        _direction.x = Input.GetAxis("Strafe") * _acceleration;

        _direction.x *= _acceleration;

        if (_controller.isGrounded)
        {
            _direction.y = 0.0f;
        }
        else
        {
            _direction.y = -Physics.gravity.magnitude * _gravityFactor;
        }

        // Movement velocity section
        _movement += _direction * Time.fixedDeltaTime;

        _movement.z = _direction.z == 0.0f ? 0.0f :
            Mathf.Clamp(_movement.z, -_moveSpeed * _acceleration,
            _moveSpeed * _acceleration);

        _movement.x = _direction.x == 0.0f ? 0.0f :
            Mathf.Clamp(_movement.x, -_moveSpeed * _acceleration,
            _moveSpeed * _acceleration);
    }

    /// <summary>
    /// Method for concrete movement of the player's avatar
    /// </summary>
    private void PlayerMovement()
    {
        if (!_canMove) return;
        #region MaxVelocityConditions
        if (_movement.x > MAX_POSITIVE_MOVEMENT_VELOCITY)
        {
            _movement.x = MAX_POSITIVE_MOVEMENT_VELOCITY;
        }

        if (_movement.x < MAX_NEGATIVE_MOVEMENT_VELOCITY)
        {
            _movement.x = MAX_NEGATIVE_MOVEMENT_VELOCITY;
        }

        if (_movement.z > MAX_POSITIVE_MOVEMENT_VELOCITY)
        {
            _movement.z = MAX_POSITIVE_MOVEMENT_VELOCITY;
        }

        if (_movement.z < MAX_NEGATIVE_MOVEMENT_VELOCITY)
        {
            _movement.z = MAX_NEGATIVE_MOVEMENT_VELOCITY;
        }
        #endregion

        Vector3 playerMovement = _movement * Time.fixedDeltaTime;

        if (_controller.enabled)
            _controller.Move(transform.TransformVector(playerMovement));
    }
    #endregion

    /// <summary>
    /// Method to change value of _canMove and stop concrete movement if false
    /// </summary>
    protected void ToggleMovement()
    {
        _canMove = !_canMove;
    }

    /// <summary>
    /// Method to make a camera active or not
    /// </summary>
    protected void ToggleCamera()
    {
        _cameraActive = !_cameraActive;
    }

    /// <summary>
    /// Method to make vCam enable or disable it
    /// </summary>
    public void DisableVcam()
    {
        _cameraMachine.enabled = !_cameraMachine.enabled;
    }

    /// <summary>
    /// Method to interact with Action Man game object
    /// </summary>
    public void ActionManInteraction()
    {
        DisableVcam();
        _actionManUse = !_actionManUse;
        ToggleCamera();
        ToggleMovement();
    }
}
