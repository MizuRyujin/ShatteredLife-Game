﻿/// @file
/// @brief This file contains the controller for the floating text that spawns on top of
/// an interactable object.
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;
using TMPro;

/// <summary>
/// Floating text has a TMPro 3D text and gets the main active camera on start. Used to
/// put a text floating on top of an object and facing a camera.
/// </summary>
public class FloatingText : MonoBehaviour
{
    /// <summary>
    /// TMPro 3D text prefab.
    /// </summary>
    private TextMeshPro text;

    /// <summary>
    /// Camera to look at.
    /// </summary>
    private Camera cameraToLookAt;

    /// <summary>
    /// Parent transform of this object, we only want position and not rotation and scale,
    /// therefore SetParent is unusable in this case.
    /// </summary>
    private Transform _followTransform;

    /// <summary>
    /// Offset from the parent transform this text has.
    /// </summary>
    private Vector3 offsetPos;

    /// <summary>
    /// Height of the current followed object.
    /// </summary>
    private float followObjHeight;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        cameraToLookAt = Camera.main;
        text = GetComponent<TextMeshPro>();
        text.text = "E";
    }

    /// <summary>
    /// Sets a new parent to the text game object.
    /// </summary>
    /// <param name="followTransform"> Transform to follow</param>
    /// <param name="colliderHeight"> Collider height of the follow object</param>
    public void SetFollow(Transform followTransform, float colliderHeight)
    {
        _followTransform = followTransform;
        followObjHeight = colliderHeight;
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (_followTransform)
        {
            offsetPos = _followTransform.position;
            offsetPos.y += followObjHeight / 2;
            transform.position = offsetPos;
        }

        transform.LookAt(cameraToLookAt?.transform);
    }
}
