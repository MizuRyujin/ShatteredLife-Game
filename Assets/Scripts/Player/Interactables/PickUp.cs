﻿/// @file
/// @brief Contains the pick up script to interact in the game.
/// 
/// @author João Rebelo, Miguel Fernandéz, Rodrigo Pinheiro e Tomás Franco.
/// @date 2020

using UnityEngine;

/// <summary>
/// Pick up script, interacts with an object in the given layers, casting a ray
/// from the center of the camera.
/// </summary>
public class PickUp : MonoBehaviour
{
    /// <summary>
    /// Initial hand position for the player.
    /// </summary>
    [SerializeField] private Transform _inHandPos = null;

    /// <summary>
    /// Layers on where to check for a grabable object.
    /// </summary>
    [SerializeField] private LayerMask _grabObjectsLayer = default;

    /// <summary>
    /// Object scale in relation to it's original scale in the player's hand.
    /// </summary>
    [Range(0, 1)]
    [SerializeField] private float _handScale = .5f;

    /// <summary>
    /// Floating text prefab to place on top of the text.
    /// </summary>
    [SerializeField] private FloatingText _textObject = null;

    /// <summary>
    /// Pick up range of the player to detect a grabable object.
    /// </summary>
    [SerializeField] private float _pickUpRange = default;

    /// <summary>
    /// Inventory controller on where to put inventory items.
    /// </summary>
    [SerializeField] private InventoryController _inventory = null;

    /// <summary>
    /// Player that will pick up an object script.
    /// </summary>
    [SerializeField] private AbstractPlayer _thisPlayerScript = null;

    /// <summary>
    /// If the current player is the toy version of it.
    /// </summary>
    [SerializeField] private bool _isActionMan = default;

    /// <summary>
    /// Boolean variable to control if an object is in hand.
    /// </summary>
    private bool _inHand = false;

    /// <summary>
    /// Player tag name.
    /// </summary>
    private string _playerTag;

    /// <summary>
    /// Initial scale of the picked up object.
    /// </summary>
    private Vector3 initialScale;

    /// <summary>
    /// Object in hand rigidbody, so we can deactivate it.
    /// </summary>
    private Rigidbody _objInHand;

    /// <summary>
    /// Current instantiated pop up text.
    /// </summary>
    private FloatingText _currentText;

    /// <summary>
    /// Ray to be used in the raycast.
    /// </summary>
    private Ray _ray;
    
    /// <summary>
    /// Information to get from the raycast.
    /// </summary>
    private RaycastHit _other;

    /// <summary>
    /// Item if the picked up object is an inventory item.
    /// </summary>
    private Paper _item;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        _playerTag = "Player";
    }

    /// <summary>
    /// 
    /// </summary>
    private void Update()
    {

        if (Physics.Raycast(transform.position, transform.forward,
         out _other, _pickUpRange, _grabObjectsLayer))
        {
            if (_textObject && !_currentText && !_inHand)
            {
                _currentText = Instantiate(
                    _textObject,
                    _other.transform.position,
                    _other.transform.rotation);
                _currentText.SetFollow(_other.transform, _other.collider.bounds.extents.y);
            }

        }
        else if (_currentText)
            Destroy(_currentText.gameObject);

        CheckInput();
    }

    /// <summary>
    /// Check's for a player input.
    /// </summary>
    private void CheckInput()
    {
        if (Input.GetButtonDown("E"))
        {
            PickUpObj();
        }
    }

    /// <summary>
    /// Pick up object from the ground, disabling it's rigidbody.
    /// </summary>
    public void PickUpObj()
    {
        // If theres an object in hand and input pressed Drop it
        if (_inHand)
        {
            _objInHand.useGravity = true;
            _objInHand.isKinematic = false;
            _objInHand.velocity = default;
            _objInHand.transform.parent = null;
            _inHand = false;
            _objInHand.transform.localScale = initialScale;
        }

        // Grabable object
        if (_other.rigidbody)
        {
            if (!_inHand)
            {
                initialScale = _other.transform.localScale;
                _other.rigidbody.useGravity = false;
                _other.rigidbody.isKinematic = true;
                _other.rigidbody.velocity = Vector3.zero;
                _other.transform.SetParent(_inHandPos);
                _other.transform.position = _inHandPos.position;
                _other.transform.rotation = _inHandPos.rotation;
                _other.transform.localScale = initialScale * _handScale;
                _inHand = true;
            }
        }
        _objInHand = _other.rigidbody;

        // Action man interaction
        if (_other.transform?.gameObject.tag == _playerTag)
        {
            ChangePlayerObject(
                _other.collider.gameObject.GetComponent<AbstractPlayer>());
        }

        // Interactable object
        if (_other.transform != null && _other.transform.TryGetComponent(
            out Interactable interactable))
        {
            if (!_isActionMan)
            {
                if (!interactable.CompareTag("Track"))
                    interactable.Interact();
            }

            else
            {
                interactable.Interact();
            }
        }

        // Paper map piece
        if (_other.collider)
            if (_other.transform.gameObject.TryGetComponent(out Paper _item))
            {
                _inventory.AddItem(_item);
            }

    }

    /// <summary>
    /// Clears the current floating text;
    /// </summary>
    public void ClearFloatingText()
    {
        if (_currentText)
            Destroy(_currentText.gameObject);
    }

    /// <summary>
    /// Change the current playable player character.
    /// </summary>
    /// <param name="pScript"> 
    /// Player script of the targeted player character
    /// </param>
    private void ChangePlayerObject(AbstractPlayer pScript)
    {
        pScript.ActionManInteraction();
        _thisPlayerScript.ActionManInteraction();
    }

    /// <summary>
    /// Debug tool to draw the pick up ray in scene.
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(_ray.origin, _ray.direction);
    }
}