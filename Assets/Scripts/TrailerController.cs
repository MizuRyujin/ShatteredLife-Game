﻿using UnityEngine;

/// <summary>
/// Controls the game camera shaders on the trailer scene
/// </summary>
public class TrailerController : MonoBehaviour
{
    [Header("References")]
    /// <summary>
    /// Child camera shader object
    /// </summary>
    [SerializeField] private GameObject _childBrain = null;
    /// <summary>
    /// Adult camera shader object
    /// </summary>
    [SerializeField] private GameObject _adultBrain = null;
    /// <summary>
    /// Old man camera shader object
    /// </summary>
    [SerializeField] private GameObject _oldBrain = null;

    [Header("Properties")]
    /// <summary>
    /// Seconds before changing to the old man camera since scene start
    /// </summary>
    [SerializeField] private float _secsBeforeOldCam = 20.0f;
    /// <summary>
    /// Seconds before changing to the adult camera since scene start
    /// </summary>
    [SerializeField] private float _secsBeforeAldultCam = 55.0f;

    /// <summary>
    /// Variable to Store start time
    /// </summary>
    private float _timeOfStart;
    /// <summary>
    /// Currently active shader brain cam object
    /// </summary>
    private GameObject _activeCam;

    /// <summary>
    /// Has the adult section triggered
    /// </summary>
    private bool _adultSection;

    /// <summary>
    /// Has the old man section triggered
    /// </summary>
    private bool _oldSection;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        _timeOfStart = Time.time;
        _activeCam = _childBrain;
    }
    
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    private void Update()
    {
        // Update adult section time
        if (Time.time - _timeOfStart > _secsBeforeAldultCam && !_adultSection)
        {
            _adultSection = true;
            ChangeActiveCam(_adultBrain);
        }
        // Update oldman section time
        else if (Time.time - _timeOfStart > _secsBeforeOldCam && !_oldSection)
        {
            _oldSection = true;
            ChangeActiveCam(_oldBrain);
        }
    }

    /// <summary>
    /// Toggles a new camera, consequentially swapping shaders
    /// </summary>
    /// <param name="newCam">Camera to change to</param>
    private void ChangeActiveCam(GameObject newCam)
    {
        _activeCam.SetActive(false);
        _activeCam = newCam;
        _activeCam.SetActive(true);
    }
}
