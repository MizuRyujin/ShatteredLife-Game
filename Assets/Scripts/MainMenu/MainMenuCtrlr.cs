﻿/// @file
/// @brief This file controlls all Main menu
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;
using TMPro;

/// <summary>
/// Main menu controller
/// </summary>
public class MainMenuCtrlr : MonoBehaviour
{
    /// <summary>
    /// Number of menu burron
    /// </summary>
    private const byte _NUMBER_OF_BUTTONS = 2;

    /// <summary>
    /// Scene to load when the start button is pressed
    /// </summary>
    private const string _GAME_SCENE_NAME = "InitialCutscene";

    [Header("Cameras")]
    /// <summary>
    /// Start option cam
    /// </summary>
    [SerializeField] private CinemachineVirtualCamera _startOptCam = null;

    /// <summary>
    /// Quit option cam
    /// </summary>
    [SerializeField] private CinemachineVirtualCamera _quitOptCam = null;

    [Header("Texts")]

    /// <summary>
    /// Start option text
    /// </summary>
    [SerializeField] private TextMeshPro _startText = null;

    /// <summary>
    /// Quit option text
    /// </summary>
    [SerializeField] private TextMeshPro _quitText = null;

    [Header("Audio Clips")]
    /// <summary>
    /// Sound to be played when buttons swap
    /// </summary>
    [SerializeField] private AudioClip _swoosh = null;

    /// <summary>
    /// Sound to be played on button selection
    /// </summary>
    [SerializeField] private AudioClip _selected = null;

    /// <summary>
    /// Object audio source
    /// </summary>
    private AudioSource _audioSource;

    /// <summary>
    /// Current button index
    /// </summary>
    private int _curIndex;
    
    /// <summary>
    /// Every menu button
    /// </summary>
    private MainMenuButton[] _buttons;

    /// <summary>
    /// Start pressed Action
    /// </summary>
    private Action StartPressed;

    /// <summary>
    /// Quit pressed Action
    /// </summary>
    private Action QuitPressed;

    /// <summary>
    /// Awake initializes and assigns variables
    /// </summary>
    private void Awake()
    {
        // Hide Cursor
        Cursor.visible = false;

        // Get this object audio source
        _audioSource = GetComponent<AudioSource>();
    
        _curIndex = 0;
        _buttons = new MainMenuButton[_NUMBER_OF_BUTTONS];

        StartPressed += LoadGameScene;
        QuitPressed += QuitGame;

        _buttons[0] =
            new MainMenuButton(_startText, _startOptCam, StartPressed);
        _buttons[1] =
            new MainMenuButton(_quitText, _quitOptCam, QuitPressed);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateInputs();
    }

    /// <summary>
    /// Update input reading
    /// </summary>
    private void UpdateInputs()
    {
        // Positive input
        if (Input.GetKeyDown(KeyCode.W) || 
            Input.GetKeyDown(KeyCode.UpArrow) || 
            Input.GetKeyDown(KeyCode.D))
            IndexDown();
        
        // Negative input
        else if (Input.GetKeyDown(KeyCode.S) || 
                Input.GetKeyDown(KeyCode.DownArrow) || 
                Input.GetKeyDown(KeyCode.A))
            IndexUp();

        // Selection input
        else if (Input.GetKeyDown(KeyCode.Space) || 
                Input.GetKeyDown(KeyCode.Return))
            SubmitIntention();
    }

    /// <summary>
    /// Button index UP
    /// </summary>
    private void IndexUp()
    {
        // Increment index
        if (++_curIndex >= _NUMBER_OF_BUTTONS)
            _curIndex = 0;
        UpdateButtons();
    }

    /// <summary>
    /// Button Index Down
    /// </summary>
    private void IndexDown()
    {
        // Decrement index
        if (--_curIndex < 0)
            _curIndex = _NUMBER_OF_BUTTONS - 1;
        UpdateButtons();
    }

    /// <summary>
    /// Update button states
    /// </summary>
    private void UpdateButtons()
    {
        for (int i = 0; i < _NUMBER_OF_BUTTONS; i++)
            if (i != _curIndex)
                _buttons[i].Deselect();
            else
                _buttons[i].Selected();

        _audioSource.PlayOneShot(_swoosh, 4);
    }

    /// <summary>
    /// Press the button of the current index
    /// </summary>
    private void SubmitIntention()
    {
        _buttons[_curIndex].Press();
    }

    /// <summary>
    /// Load game scene
    /// </summary>
    private void LoadGameScene()
    {
        SceneManager.LoadScene(_GAME_SCENE_NAME);
    }

    /// <summary>
    /// Quit the game
    /// </summary>
    private void QuitGame()
    {
        Application.Quit();
    }
}
