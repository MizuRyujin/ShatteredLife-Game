﻿/// @file
/// @brief This file holds main menu button actions
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using System;
using Cinemachine;
using TMPro;

/// <summary>
/// An independent menu button
/// </summary>
public struct MainMenuButton
{
    /// <summary>
    /// Menu text
    /// </summary>
    private TextMeshPro _textPro;
    /// <summary>
    /// Menu directional camera
    /// </summary>
    private CinemachineVirtualCamera _cam;
    /// <summary>
    /// Action when the button is pressed
    /// </summary>
    private Action _pressedAction;

    /// <summary>
    /// Button constructor
    /// </summary>
    /// <param name="tmProText">Button text</param>
    /// <param name="cineCam">Button cam</param>
    /// <param name="pressedAction">Button pressed action</param>
    public MainMenuButton(TextMeshPro tmProText,
    CinemachineVirtualCamera cineCam, Action pressedAction)
    {
        _textPro = tmProText;
        _cam = cineCam;
        _pressedAction = pressedAction;
    }

    /// <summary>
    /// The button is currently selected
    /// </summary>
    public void Selected()
    {
        _textPro.gameObject.SetActive(true);
        _cam.enabled = true;
    }

    /// <summary>
    /// The button is no longer selected
    /// </summary>
    public void Deselect()
    {
        _textPro.gameObject.SetActive(false);
        _cam.enabled = false;
    }

    /// <summary>
    /// Button was pressed
    /// </summary>
    public void Press()
    {
        OnPress();
    }

    /// <summary>
    /// Call Pressed Action
    /// </summary>
    private void OnPress()
    {
        _pressedAction?.Invoke();
    }

}
