﻿/// @file
/// @brief File contains GameProcessVolumes which is an entry point to access the
/// cameras currently used post process volumes.

using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[CreateAssetMenu(fileName = "GameVolumes", menuName = "ShatteredLife/Effects", order = 4)]
/// <summary>
/// GameProcessVolumes acts as an entry point to the camera's post-process
/// volumes, used to change the active volumes in runtime.
/// </summary>
public class GameProcessVolumes : ScriptableObject
{
   /// <summary>
   /// Current outline volume.
   /// </summary>
   /// <value> PostProcessVolume curresponding to the Outline effect</value>
   public PostProcessVolume Outline {get; set;}

   /// <summary>
   /// Current ColorScale volume.
   /// </summary>
   /// <value> PostProcessVolume curresponding to the ColorScale effect</value>
   public PostProcessVolume ColorScale {get; set;}
}
