﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// MainMenu, first scene in game
/// </summary>
public class MainMenu : MonoBehaviour
{
    /// <summary>
    /// Load initial cutscene to start game
    /// </summary>
    public void LoadMainScene()
    {
        // Load main scene by index
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    /// <summary>
    /// Quits game
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }
}
