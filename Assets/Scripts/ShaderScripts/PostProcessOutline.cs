﻿/// @file
/// @brief This file contains the script to add the Outline shader to the
/// HLSL post-processing stack for unity.
/// 
/// @author João Rebelo, Miguel Fernandéz, Rodrigo Pinheiro e Tomás Franco.
/// @date 2020
/// 
using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

// Script has the base from https://github.com/Unity-Technologies/PostProcessing/wiki/Writing-Custom-Effects
[Serializable]
[PostProcess(typeof(PostProcessOutlineRenderer), PostProcessEvent.BeforeStack, "PineTree/Post Process Outline")]
/// <summary>
/// Post-process outline contains the information and settings to be used by the 
/// post-processing stack.
/// </summary>
public sealed class PostProcessOutline : PostProcessEffectSettings
{
    [Tooltip("Number of pixels between samples that are tested for an edge. When this value is 1, tested samples are adjacent.")]
    /// <summary>
    /// Number of pixels between samples that are tested for an edge. 
    /// When this value is 1, tested samples are adjacent.
    /// </summary>
    /// <value> Scale to increase the pixels</value>
    public IntParameter scale = new IntParameter { value = 1 };
    /// <summary>
    /// Color parameter to color the outline.
    /// </summary>
    /// <value> Color of the outline</value>
    public ColorParameter color = new ColorParameter { value = Color.white };
    [Tooltip("Difference between depth values, scaled by the current depth, required to draw an edge.")]
    /// <summary>
    /// Difference between the depth values, scaled by the current depth,
    /// required to draw an edge.
    /// </summary>
    /// <value> Float value of depth distance</value>
    public FloatParameter depthThreshold = new FloatParameter { value = 1.5f };
    [Range(0, 1), Tooltip("The value at which the dot product between the surface normal and the view direction will affect " +
        "the depth threshold. This ensures that surfaces at right angles to the camera require a larger depth threshold to draw " +
        "an edge, avoiding edges being drawn along slopes.")]
    /// <summary>
    /// The value at which the dot product between the surface normal 
    /// and the view direction will affect the depth threshold.
    /// This ensures that surfaces at right angles to the camera require a larger depth threshold to draw
    /// an edge, avoiding edges being drawn along slopes.
    /// </summary>
    /// <value> Dot product treshold</value>
    public FloatParameter depthNormalThreshold = new FloatParameter { value = 0.5f };
    [Tooltip("Scale the strength of how much the depthNormalThreshold affects the depth threshold.")]
    /// <summary>
    /// Scale the strength of how much the depthNormalThreshold affects the depth threshold
    /// </summary>
    /// <value> Strength value of the normals effect</value>
    public FloatParameter depthNormalThresholdScale = new FloatParameter { value = 7 };
    [Range(0, 1), Tooltip("Larger values will require the difference between normals to be greater to draw an edge.")]
    /// <summary>
    /// Larger values will require the difference between normals to be greater to draw an edge.
    /// </summary>
    /// <value> Normal treshold value for the difference between normals in
    /// two adjacent faces
    /// </value>
    public FloatParameter normalThreshold = new FloatParameter { value = 0.4f };    
}

/// <summary>
/// PostProcessOutlineRenderer sets the settings from the user input in the inspector
/// and tells the post-processing stack how to blit the image to the camera.
/// </summary>
public sealed class PostProcessOutlineRenderer : PostProcessEffectRenderer<PostProcessOutline>
{
    /// <summary>
    /// Render method on how the post-process will use the effect to render.
    /// </summary>
    /// <param name="context"> Context variable of the post-processing stack</param>
    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/PineTree/Outline Post Process"));
        sheet.properties.SetFloat("_Scale", settings.scale);
        sheet.properties.SetColor("_Color", settings.color);
        sheet.properties.SetFloat("_DepthThreshold", settings.depthThreshold);
        sheet.properties.SetFloat("_DepthNormalThreshold", settings.depthNormalThreshold);
        sheet.properties.SetFloat("_DepthNormalThresholdScale", settings.depthNormalThresholdScale);
        sheet.properties.SetFloat("_NormalThreshold", settings.normalThreshold);
        sheet.properties.SetColor("_Color", settings.color);

        Matrix4x4 clipToView = GL.GetGPUProjectionMatrix(context.camera.projectionMatrix, true).inverse;
        sheet.properties.SetMatrix("_ClipToView", clipToView);

        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}