﻿/// @file
/// @brief This file contains the void door control script, which controls the
/// material values for a room's door.
/// 
/// @author João Rebelo, Miguel Fernandéz, Rodrigo Pinheiro e Tomás Franco.
/// @date 2020
using System.Collections;
using UnityEngine;

/// <summary>
/// Controls the void door dissolve shader
/// </summary>
public class VoidDoorControl : MonoBehaviour
{
    [Tooltip("Don't mess with this name, or go to the DissolveScroll shader and" +
        " double check the variable names")]
    [SerializeField]
    /// <summary>
    /// Variable name inside shader code to access the dissolve variable.
    /// </summary>
    private string dissolveVariableName = default;
    
    [Tooltip("Don't mess with this names, or go to the DissolveScroll shader and" +
        " double check the variable names")]
    [SerializeField]
    /// <summary>
    /// Variable name inside shader code to access the edge variable.
    /// </summary>
    private string edgeVariableName = default;
    
    [Tooltip("How fast you want to dissolve")]
    [SerializeField]
    /// <summary>
    /// Speed in which the door will open/close.
    /// </summary>
    private float dissolveSpeed = default;
    
    [Tooltip("Box collision of the door")]
    [SerializeField]
    /// <summary>
    /// Collider used by the door so we can de-activate it when it closes.
    /// </summary>
    private Collider col = null;

    /// <summary>
    /// Renderer used by the void door object to access it's materials.
    /// </summary>
    private Renderer rd = default;
    
    /// <summary>
    /// Variable to control de dissolve state of the door.
    /// </summary>
    private float dissolve = default;

    /// <summary>
    /// Variable that controls audio output
    /// </summary>
    private AudioSource _audioSource = default;

    /// <summary>
    /// Bool to check if playing, turns true on enable audioSource
    /// </summary>
    private bool _playing = default;

    /// <summary>
    /// Door's current state, false for when it's open and true when it's closed
    /// </summary>
    public bool State {get; private set;}
    
    #region debug tools
    [SerializeField] private bool Dissolve = default;
    private void Update()
    {
        if (Dissolve) 
        {
            Dissolve = false;
            StartCoroutine(ToggleDissolve());
        }

        Sound(State);
    }
    #endregion
    
    private void Awake()
    {
        rd = GetComponentInChildren<Renderer>();
        rd.material.SetFloat(dissolveVariableName, 0f);
        rd.material.SetFloat(edgeVariableName, 0f);
        // False if dissolved, true if visible
        State = true;
        dissolve = 0f;

        // Sound
        _audioSource = GetComponent<AudioSource>();
        _playing = true;
    }

    /// <summary>
    /// Calls the ToggleDissolve coroutine to be used on event.
    /// </summary>
    public void DissolveRoutine() 
    {
        StartCoroutine(ToggleDissolve());
    }

    /// <summary>
    /// Dissolve animation coroutine to open/close the door.
    /// </summary>
    /// <returns> Waits for next frame.</returns>
    private IEnumerator ToggleDissolve() 
    {
        if (State && dissolve != 1)
        {
            while (dissolve < 1f)
            {
                rd.material.SetFloat(dissolveVariableName, dissolve);
                rd.material.SetFloat(edgeVariableName, dissolve);
                dissolve += Time.deltaTime * dissolveSpeed;
                yield return null;
            }
            // Now it's dissolved so disable collision
            col.enabled = false;
            // Just to make sure set both parameters to 1 so we don't see any
            // artifacts
            rd.material.SetFloat(dissolveVariableName, 1f);
            rd.material.SetFloat(edgeVariableName, 1f);
        }
        else if (!State && dissolve >= 1f)
        {
            while (dissolve >= 0f)
            {
                rd.material.SetFloat(dissolveVariableName, dissolve);
                rd.material.SetFloat(edgeVariableName, dissolve);
                dissolve -= Time.deltaTime * dissolveSpeed;
                yield return null;
            }
            // Now it's visible so turn the collisions on
            col.enabled = true;
            // Just to make sure set both parameters to 0 so we don't see any
            // artifacts
            rd.material.SetFloat(dissolveVariableName, 0f);
            rd.material.SetFloat(edgeVariableName, 0f);
        }
        State = !State;
    }

    /// <summary>
    /// Enables or disables sound depending on the state
    /// </summary>
    /// <param name="state"> Accepts a state to control sound </param>
    private void Sound(bool state)
    {
        if (!state)
        {
            _audioSource.Pause();
        }
        else
        {
            _audioSource.UnPause();
        }
    }
}
