﻿/// @file
/// @brief This file contains Normals Texture render script, which creates a
/// new camera with the normals currently rendered by the camera.
/// 
/// @date 2020
/// @author Rodrigo Pinheiro
using UnityEngine;

/// <summary>
/// Creates a new camera rendering the normals of every object inside the camera
/// frustum.
/// </summary>
public class RenderReplacementShaderToTexture : MonoBehaviour
{
    /// <summary>
    /// Shader to use in order to get the normals being rendered.
    /// </summary>
    [SerializeField]
    Shader replacementShader = null;

    /// <summary>
    /// Texture format of the final image being rendered.
    /// </summary>
    [SerializeField]
    RenderTextureFormat renderTextureFormat = RenderTextureFormat.ARGB32;

    /// <summary>
    /// How to sample the texture.
    /// </summary>
    [SerializeField]
    FilterMode filterMode = FilterMode.Point;

    /// <summary>
    /// The max camera depth where to get the normals
    /// </summary>
    [SerializeField]
    int renderTextureDepth = 24;

    /// <summary>
    /// Unity camera clear flags.
    /// </summary>
    [SerializeField]
    CameraClearFlags cameraClearFlags = CameraClearFlags.Color;

    /// <summary>
    /// Unity camera background clear color.
    /// </summary>
    [SerializeField]
    Color background = Color.black;

    /// <summary>
    /// Target texture variable name inside the replacementShader Shader.
    /// </summary>
    [SerializeField]
    string targetTexture = "_RenderTexture";

    /// <summary>
    /// Final render texture taken from the shader.
    /// </summary>
    private RenderTexture renderTexture;

    /// <summary>
    /// Camera to be created
    /// </summary>
    private new Camera camera;

    /// <summary>
    /// Creates a new camera, gets the renderTexture from the main camera
    /// using the replacement shader and then set's all the variables in the
    /// secondary camera.
    /// </summary>
    private void Start()
    {
        Camera thisCamera = GetComponent<Camera>();

        // Create a render texture matching the main camera's current dimensions.
        renderTexture = new RenderTexture
            (thisCamera.pixelWidth, thisCamera.pixelHeight,
            renderTextureDepth, renderTextureFormat);
        renderTexture.filterMode = filterMode;
        
        // Surface the render texture as a global variable, available to all shaders.
        Shader.SetGlobalTexture(targetTexture, renderTexture);

        // Setup a copy of the camera to render the scene using the normals shader.
        GameObject copy = new GameObject("Camera" + targetTexture);
        camera = copy.AddComponent<Camera>();
        camera.CopyFrom(thisCamera);
        camera.transform.SetParent(transform);
        camera.targetTexture = renderTexture;
        camera.SetReplacementShader(replacementShader, "RenderType");
        camera.depth = thisCamera.depth - 1;
        camera.clearFlags = cameraClearFlags;
        camera.backgroundColor = background;
        camera.fieldOfView = thisCamera.fieldOfView;
    }
}
