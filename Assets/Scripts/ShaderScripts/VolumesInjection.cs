﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class VolumesInjection : MonoBehaviour
{
    [SerializeField] private PostProcessVolume _outline;
    [SerializeField] private PostProcessVolume _colorScale;
    [SerializeField] private GameProcessVolumes _volumes;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        _volumes.Outline = _outline;
        _volumes.ColorScale = _colorScale;
    }
}
