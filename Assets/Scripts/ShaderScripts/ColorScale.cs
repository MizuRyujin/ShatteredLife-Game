﻿/// @file
/// @brief This file contains the script to add the Color scale shader to the
/// HLSL post-processing stack for unity.
/// 
/// @author João Rebelo, Miguel Fernandéz, Rodrigo Pinheiro e Tomás Franco.
/// @date 2020

using System;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine;

// Script has the base from https://github.com/Unity-Technologies/PostProcessing/wiki/Writing-Custom-Effects
[Serializable]
[PostProcess(typeof(PostProcessColorScaleRenderer), PostProcessEvent.BeforeStack, "PineTree/Color Scale")]
/// <summary>
/// Color scale contains the information and settings to be used by the post
/// -processing stack.
/// </summary>
public sealed class ColorScale : PostProcessEffectSettings
{
    // Insert post process settings here
    [Range(0f, 1f), Tooltip("Colorscale effect intensity.")]
    /// <summary>
    /// Float value for the blend parameter between the real colors and the 
    /// picked colors.
    /// </summary>
    /// <value> Float value percentage to blend to</value>
    public FloatParameter blend = new FloatParameter { value = 0.5f };
    /// <summary>
    /// Darkest color to switch to form the grayscale texture.
    /// </summary>
    /// <value> Color</value>
    public ColorParameter darkest = new ColorParameter { value = Color.black};
    /// <summary>
    /// Mid dark color to switch to form the grayscale texture.
    /// </summary>
    /// <value> Color</value>
    public ColorParameter dark = new ColorParameter { value = Color.grey};
    /// <summary>
    /// Midtones color to switch to form the grayscale texture.
    /// </summary>
    /// <value> Color</value>
    public ColorParameter midtones = new ColorParameter { value = Color.grey};
    /// <summary>
    /// Light color to switch to form the grayscale texture.
    /// </summary>
    /// <value> Color</value>
    public ColorParameter light = new ColorParameter { value = Color.yellow};
    /// <summary>
    /// Lightest color to switch to form the grayscale texture.
    /// </summary>
    /// <value> Color</value>
    public ColorParameter lightest = new ColorParameter { value = Color.white};

    /// <summary>
    /// Method to disable the effect given a blend context from the post-process
    /// volume.
    /// </summary>
    /// <param name="context"> Param given by the inspector blend variable</param>
    /// <returns> Boolean to turn on/off the effect</returns>
    public override bool IsEnabledAndSupported(PostProcessRenderContext context)
    {
        // Turns off the effect if blend value is 0
        return enabled.value && blend.value > 0f;
    }
}

/// <summary>
/// PostProcessColorScale sets the settings from the user input in the inspector
/// and tells the post-processing stack how to blit the image to the camera.
/// </summary>
public sealed class PostProcessColorScaleRenderer : PostProcessEffectRenderer<ColorScale>
{
    /// <summary>
    /// Render method on how the post-process will use the effect to render.
    /// </summary>
    /// <param name="context"> Context variable of the post-processing stack</param>
    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/PineTree/ColorScale"));
        // Set values here
        sheet.properties.SetFloat("_Blend", settings.blend);
        sheet.properties.SetColor("_SDarkest", settings.darkest);
        sheet.properties.SetColor("_SDark", settings.dark);
        sheet.properties.SetColor("_SMidtones", settings.midtones);
        sheet.properties.SetColor("_SLight", settings.light);
        sheet.properties.SetColor("_SLightest", settings.lightest);

        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}