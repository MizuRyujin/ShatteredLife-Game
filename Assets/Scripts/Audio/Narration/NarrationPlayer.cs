﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrationPlayer : MonoBehaviour
{
    private AudioSource _playerNarrationSource = null;
    [SerializeField] private MaskedNarrationSingleton _instance = null;

    void Awake()
    {
        _instance._narrationPlayer = this;
        _playerNarrationSource = GetComponent<AudioSource>();    
        _playerNarrationSource.loop = false;    
    }

    public void PlayNarration(AudioClip narrationClip)
    {
        _playerNarrationSource.clip = narrationClip;
        _playerNarrationSource.Play();
    }
}
