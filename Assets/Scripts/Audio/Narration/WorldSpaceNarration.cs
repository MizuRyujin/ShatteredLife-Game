﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class WorldSpaceNarration : NarrationObject
{
    [Tooltip("Gets all siblings and destroys them when done.")]
    [SerializeField] private bool _useChain;
    private GameObject[] _chain;

    private Collider _collider;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        _collider.isTrigger = true;

        if (_useChain)
        {
            _chain = new GameObject[transform.parent.childCount - 1];

            int dummy = 0;
            foreach (Transform ct in transform.parent)
            {
                if(ct != this.transform)
                {
                    _chain[dummy] = ct.gameObject;
                    dummy++;
                }
            }
        }
    }

    private void DestroySelf()
    {
        if (_useChain)
            foreach(GameObject go in _chain)
                Destroy(go);
                
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == _PLAYER_TAG)
        {
            NarrationPlayer np = other.GetComponent<NarrationPlayer>();
            PlaySound();
            DestroySelf();
        }
    }
}
