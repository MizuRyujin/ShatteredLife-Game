﻿using UnityEngine;

public abstract class NarrationObject : MonoBehaviour
{
    protected const string _PLAYER_TAG = "Player";

    [SerializeField] private MaskedNarrationSingleton _narrationPlayer;

    [SerializeField] private AudioClip _narrationClip;

    protected void PlaySound()
    {
        _narrationPlayer._narrationPlayer?.PlayNarration(_narrationClip);
    }
}
