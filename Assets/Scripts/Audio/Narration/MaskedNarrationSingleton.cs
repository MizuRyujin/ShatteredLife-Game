﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ShatteredLife/Masked Narration Singleton")]
public class MaskedNarrationSingleton : ScriptableObject
{
    public NarrationPlayer _narrationPlayer {get; set;}
}
