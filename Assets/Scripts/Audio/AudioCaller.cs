﻿/// @file
/// @brief This file contains the audio manger object for the game.
/// 
/// @author Rodrigo Pinheiro
/// @date 2020

using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;

/// <summary>
/// Scrip-table object that contains references to music and mixer groups and a
/// callable method to play sound bites and sound effects.
/// </summary>
[CreateAssetMenu(fileName = "Audio Manager", menuName = "ShatteredLife/Audio", order = 2)]
public class AudioCaller : ScriptableObject
{
    /// <summary>
    /// Music clips to play
    /// </summary>
    [SerializeField] private AudioClip[] musicClips = default;
    /// <summary>
    /// Ambiance Clips to play
    /// </summary>
    [SerializeField] private AudioClip[] ambianceClips = default;
    /// <summary>
    /// Music mixer group
    /// </summary>
    [SerializeField] private AudioMixerGroup musicMixerGroup = default;
    /// <summary>
    /// Sound effects and sound bites mixer group.
    /// </summary>
    [SerializeField] private AudioMixerGroup sfxMixerGroup = default;

    /// <summary>
    /// Sound effects audio source prefab for 3D fall-of
    /// </summary>
    [SerializeField] private AudioSource sfxAudioSourcePrefab = default;

    /// <summary>
    /// Music audio source.
    /// </summary>
    private AudioSource musicSource = default;

    /// <summary>
    /// Ambiance sound source.
    /// </summary>
    private AudioSource ambianceSource = default;

    /// <summary>
    /// Dictionary that tracks all available callers and their sound list so it
    /// doesn't have to search every time someone calls a sound.
    /// </summary>
    private Dictionary<GameObject, List<AudioSource>> availableSources = default;

    /// <summary>
    /// Play's a given sound effect.
    /// </summary>
    /// <param name="wav"> clip to play</param>
    /// <param name="pitch"> pitch of the clip</param>
    /// <param name="volume"> volume of the clip</param>
    /// <param name="caller"> who is calling this clip, current game object</param>
    public void PlaySound(AudioClip wav,
        GameObject caller, float pitch = 1, float volume = 1)
    {
        if (availableSources == null)
            availableSources = new Dictionary<GameObject, List<AudioSource>>();

        if (availableSources.ContainsKey(caller))
        {
            // Search sources of the caller to find a non playing audioSource
            foreach (AudioSource a in availableSources[caller]) 
            {
                if (!a.isPlaying)
                {
                    a.clip = wav;
                    a.volume = volume;
                    a.pitch = pitch;
                    a.Play();

                    return;
                }
            }
        }
        else 
        {
            availableSources.Add(caller, new List<AudioSource>());
        }
        
        AudioSource snd = Instantiate(sfxAudioSourcePrefab, caller.transform);
        snd.transform.SetParent(caller.transform);

        snd.clip = wav;
        snd.volume = volume;
        snd.pitch = pitch;
        snd.outputAudioMixerGroup = sfxMixerGroup;

        snd.Play();

        // Reaching this point we know the source isn't in the dictionary,
        // so we add it for later use.
        availableSources[caller].Add(snd);
    }

    /// <summary>
    /// Plays a given music from the music array
    /// </summary>
    /// <param name="i"> Index in the music array to what music to play</param>
    public void PlayMusic(int i) 
    {
        if (musicSource == null) 
        {
            GameObject obj = new GameObject("Music Source");
            obj.AddComponent(typeof(AudioSource));

            musicSource = obj.GetComponent<AudioSource>();
            musicSource.outputAudioMixerGroup = musicMixerGroup;
        }
        
        musicSource.clip = musicClips[i];
        musicSource.Play();
    }

    /// <summary>
    /// Plays a given ambiance clip from the ambiance array.
    /// </summary>
    /// <param name="i"> Index for the clip in the array</param>
    public void PlayAmbience(int i)
    {
        if (ambianceSource == null)
        {
            GameObject obj = new GameObject("Music Source");
            obj.AddComponent(typeof(AudioSource));

            ambianceSource = obj.GetComponent<AudioSource>();
            ambianceSource.outputAudioMixerGroup = musicMixerGroup;
        }

        ambianceSource.clip = ambianceClips[i];
        ambianceSource.Play();
    }

    /// <summary>
    /// Stops any current music clip playing.
    /// </summary>
    public void CutMusic() 
    {
        musicSource.Stop();
    }
    /// <summary>
    /// Stops any current ambiance clip playing.
    /// </summary>
    public void CutAmbiance() 
    {
        ambianceSource.Stop();
    }
}
