﻿/// @file
/// @brief File contains Scene change interactable, which switches scenes
/// after a player interact.
/// 
/// @author Miguel Fernández, João Rebelo, Rodrigo Pinheiro, Tomás Franco.
/// @date 2020

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneSkipInteractable : Interactable
{

    [SerializeField] private int _sceneIndexToTransition = 0;

    private GameObject _player;

    private WaitForSeconds _waitTime;
    
    private PauseEffect _pause;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        _player = GameObject.Find("Player");
        _pause = _player.GetComponentInChildren<PauseEffect>();
        AddActionsToInteraction(TransitionToScene);
        _waitTime = new WaitForSeconds(2);
    }

    private void TransitionToScene()
    {
        _pause.CloseEyes();
        StartCoroutine(TransitionAnimation());
    }

    private IEnumerator TransitionAnimation()
    {
        yield return _waitTime;
        SceneManager.LoadScene(_sceneIndexToTransition);
    }
}
