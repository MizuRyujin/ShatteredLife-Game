﻿/// @file
/// @brief This file handles the initial cutscene
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Initial cutscene counts down until it is time to change to next scene
/// </summary>
public class InitialCutscene : MonoBehaviour
{
	/// <summary>
	/// Cutscene duration
	/// </summary>
	[SerializeField] private float _duration = default;

	/// <summary>
	/// Starts the Countdown corroutine
	/// </summary>
	private void Start()
	{
		StartCoroutine(CStartCountDown());
	}

	/// <summary>
	/// Waits for the cutscene time to end and loads next scene
	/// </summary>
	/// <returns>Wait time</returns>
	private IEnumerator CStartCountDown()
	{
		// Get next scene index
		byte sceneToLoad = (byte)(SceneManager.GetActiveScene().buildIndex + 1);

		// Wait
		yield return new WaitForSeconds(_duration);

		// Load scene by index
		SceneManager.LoadScene(sceneToLoad);
	}
}
