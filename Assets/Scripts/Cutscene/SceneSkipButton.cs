﻿/// @file
/// @brief This file contains a scene skipper.
/// 
/// @author Miguel Fernández, João Rebelo, Rodrigo Pinheiro, Tomás Franco.
/// @date 2020

using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// SceneSkipButton checks for a press and loads a new scene, used to skip
/// a cutscene or exit to the menu.
/// </summary>
public class SceneSkipButton : MonoBehaviour
{
    /// <summary>
    /// Scene integer to change to.
    /// </summary>
    [SerializeField] private int scene;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(scene);
        }
    }
}
