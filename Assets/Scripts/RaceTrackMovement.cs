﻿/// @file
/// @brief Contains race track ride behaviour
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Class responsible for the movement of virtual cameras with dolly tracks
/// </summary>
public class RaceTrackMovement : MonoBehaviour
{
    /// <summary>
    /// Reference for a cinemachine virtual camera
    /// </summary>
    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera _vcam = null;

    /// <summary>
    /// Reference for a cinemachine dolly cart
    /// </summary>
    [SerializeField]
    private Cinemachine.CinemachineDollyCart _cart = null;

    /// <summary>
    /// Variable to store information about created dolly track
    /// </summary>
    private Cinemachine.CinemachineTrackedDolly _track;

    /// <summary>
    /// Awake is called before any frame is rendered
    /// </summary>
    private void Awake()
    {
        _track = _vcam.GetCinemachineComponent<
            Cinemachine.CinemachineTrackedDolly>();
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        _track.m_PathPosition = _cart.m_Position;
    }
}
