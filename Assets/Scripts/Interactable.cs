﻿/// @file
/// @brief This file contains specific behaviours for the player
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;
using System;

/// <summary>
/// Abstract class to be inherited by all interactable objects
/// </summary>
public abstract class Interactable : MonoBehaviour
{
	/// <summary>
	/// Method to be used in case object isn't to be interacted again after use
	/// </summary>
	protected void RemoveScriptFromObject()
	{
		Destroy(this);
	}

	/// <summary>
	/// Method to remove an object from Grabable Object layer
	/// </summary>
	protected void ResetLayer()
	{
		gameObject.layer = 0;
	}

	/// <summary>
	/// Method to add method to Interaction event Action
	/// </summary>
	/// <param name="interactions"> Methods to be added </param>
	protected void AddActionsToInteraction(params Action[] interactions) 
	{
		for (int i = 0; i < interactions.Length; i++)
		{
			Interaction += interactions[i];
		}
	}

	/// <summary>
	/// Method to invoke Interaction Action
	/// </summary>
	public void Interact()
    {
        Interaction?.Invoke();
    }

	/// <summary>
	/// Delegate for interactions of game objects
	/// </summary>
    public event Action Interaction;
}
