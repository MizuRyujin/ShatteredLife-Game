﻿/// @file
/// @brief Contains behavior of interaction with hidden door in old room prefab 
/// 
/// @author Miguel Fernández, João Rebelo, Rodrigo Pinheiro, Tomás Franco.
/// @date 2020
using System.Collections;
using UnityEngine;

/// <summary>
/// Class responsible for door behaviour
/// </summary>
public class OldManDoor : Interactable
{
    /// <summary>
    /// Editor variable to define seconds value to be used in WaitForSeconds
    /// variable
    /// </summary>
    [SerializeField]
    private float _secToWait = default;

    /// <summary>
    /// Reference to player script
    /// </summary>
    private GameObject _player = default;

    /// <summary>
    /// Reference to player player's CharacterController
    /// </summary>
    private CharacterController _pCC = default;

    /// <summary>
    /// Reference to the player's pause effect overlay, used to hide the
    /// teleportation from the player.
    /// </summary>
    private PauseEffect _playerPause;

    /// <summary>
    /// Time value (in seconds) to be used in a coroutine
    /// </summary>
    private WaitForSeconds _seconds = default;

    /// <summary>
    /// Reference to game post process volumes that need to changed
    /// on interaction.
    /// </summary>
    [SerializeField] private GameProcessVolumes _gameVolumes;

    /// <summary>
    /// Reference to house controller script to clean map inventory after
    /// interaction with door and player teleportation
    /// </summary>
    private HouseCtrlr _houseControl;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        _seconds = new WaitForSeconds(_secToWait);
        _player = GameObject.Find("Player");
        _pCC = _player.GetComponent<CharacterController>();
        _playerPause = _player.GetComponentInChildren<PauseEffect>();
        _houseControl = GameObject.FindGameObjectWithTag("HouseControl").
            GetComponent<HouseCtrlr>();
        
        AddActionsToInteraction(TeleportPlayer);
    }

    /// <summary>
    /// Method to be used in a event Action and teleport player
    /// to some position
    /// </summary>
    private void TeleportPlayer()
    {
        _playerPause.CloseEyes();
        StartCoroutine(WaitBeforeTeleport());
        _pCC.height = _pCC.height / 2;
        _houseControl.ClearHouseCells();
    }

    /// <summary>
    /// Coroutine to have an animation play before teleportation of player
    /// </summary>
    /// <returns> Time to wait before teleport </returns>
    private IEnumerator WaitBeforeTeleport()
    {
        _pCC.enabled = false;
        yield return _seconds;
        _player.transform.position = Vector3.zero;
        _pCC.enabled = true;

        ChangeToChildCam();
        
    }

    /// <summary>
    /// Method to change outline width in PostProcessMainCamera
    /// </summary>
    private void ChangeToChildCam()
    {
        _gameVolumes.Outline.isGlobal = true;
    }
}
