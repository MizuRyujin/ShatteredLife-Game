﻿/// @file
/// @brief File contains A trigger to block the player from going back.
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Trigger to shut player inside room
/// </summary>
public class OldManTrigger : MonoBehaviour
{
    /// <summary>
    /// Mesh corresponding to this object
    /// </summary>
    private MeshRenderer _doorMesh;

    /// <summary>
    /// Collider to activate door 
    /// </summary>
    [SerializeField] private Collider _collider;

    /// <summary>
    /// Played as the door closes
    /// </summary>
    [SerializeField] private AudioClip _closeSound = default;

    /// <summary>
    /// Instance of AudioCaller - Audio Manager
    /// </summary>
	[SerializeField] private AudioCaller _audioManager = default;

    /// <summary>
    /// If the sound has been triggered before.
    /// </summary>
    private bool played;

    /// <summary>
    /// Awake to assign mesh
    /// </summary>
    private void Awake()
    {
        _doorMesh = GetComponent<MeshRenderer>();
    }

    /// <summary>
    /// Trigger to enable colliders and children
    /// </summary>
    private void OnTriggerEnter()
    {
        _doorMesh.enabled = true;
        _collider.enabled = true;

        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
        
        if (!played)
        {
            _audioManager.PlaySound(_closeSound, gameObject, 0.5f, 1f);
            played = true;
        }
    }
}
