﻿/// @file
/// @brief Contains behavior of interaction with coffee mug in old room prefab
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Class responsible for coffee mug behaviour
/// </summary>
public class OldManMug : Interactable
{
	/// <summary>
	/// Reference for the paper prefab within the old man room prefab
	/// </summary>
	[SerializeField] 
	private GameObject _targetPaper = null;

	/// <summary>
	/// Reference to the coffee inside the mug
	/// </summary>
	[SerializeField]
	private GameObject _coffeeInside = null;

    /// <summary>
    /// Drink sound to be played
    /// </summary>
    [SerializeField] private AudioClip _drinkSound = default;

    /// <summary>
    /// Instance of AudioCaller - Audio Manager
    /// </summary>
	[SerializeField] private AudioCaller _audioManager = default;

	/// <summary>
	/// Reference to the audio to be played after interacting with the mug
	/// </summary>
	[SerializeField] private InteractionNarration _narrationScript = null;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	private void Awake()
	{
		AddActionsToInteraction(
			EnablePaper,
			DisableCoffee,
			ResetLayer,
			RemoveScriptFromObject,
			PlayNarration);
	}

	/// <summary>
	/// Method to disable the coffee game object inside the mug after
	/// interaction with the mug
	/// </summary>
	private void DisableCoffee()
	{
		_coffeeInside.SetActive(false);
		
		_audioManager.PlaySound(_drinkSound, gameObject);
	}

	/// <summary>
	/// Method to enable the hidden paper in the old room prefab
	/// </summary>
	private void EnablePaper()
	{
		_targetPaper.SetActive(true);
	}

	/// <summary>
	/// Method to play the narration audio clip associated with the mug
	/// </summary>
	private void PlayNarration()
	{
		_narrationScript.PlayClip();
	}
}
