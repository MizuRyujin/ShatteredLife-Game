﻿/// @file
/// @brief 
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Class to make an object float animation
/// </summary>
public class Float : MonoBehaviour
{
    /// <summary>
    /// 
    /// </summary>
    [SerializeField] private float _maxDistance = default;

    /// <summary>
    /// 
    /// </summary>
    [SerializeField] private float _velocity = default;

    /// <summary>
    /// 
    /// </summary>
    private Vector3 _move = default;

    /// <summary>
    /// 
    /// </summary>
    private Vector3 _initPos = default;

    /// <summary>
    /// Assign initial position
    /// </summary>
    private void Start()
    {
        _initPos = transform.position;
        _move = _initPos;
    }

    /// <summary>
    /// Update method, calls move method
    /// </summary>
    private void Update()
    {
        Move();
    }

    /// <summary>
    /// Moves object in a floaty way
    /// </summary>
    private void Move()
    {
        _move = transform.position;

        _move.x =
            _initPos.x + _maxDistance * Mathf.Sin(Time.time * _velocity);

        _move.y =
            _initPos.y + _maxDistance * Mathf.Sin(Time.time * _velocity /2);

        transform.position = _move;
    }
}
