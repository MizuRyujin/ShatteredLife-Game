﻿/// @file
/// @brief This file contains a trigger used to call a function that toggles
/// an event.
/// 
/// @author João Rebelo, Miguel Fernandéz, Rodrigo Pinheiro e Tomás Franco.
/// @date 2020

using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Trigger for toggles based on Unity events.
/// </summary>
public class VoidDoorTrigger : MonoBehaviour
{
    /// <summary>
    /// Event to be called.
    /// </summary>
    [SerializeField] private UnityEvent doorEvent = default;
    /// <summary>
    /// The other collider that enters the trigger.
    /// </summary>
    private Collider other = default;
    /// <summary>
    /// If the enter event has been triggered.
    /// </summary>
    private bool triggered = default;

    /// <summary>
    /// This function is called every fixed framerate frame,
    ///  if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        if(triggered && !other)
        {
            doorEvent.Invoke();
            triggered = false;       
        }
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerEnter(Collider other)
    {
        doorEvent.Invoke();
    }

    /// <summary>
    /// OnTriggerStay is called once per frame for every Collider other
    /// that is touching the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerStay(Collider other)
    {
        this.other = other;        
        triggered = true;
    }
}
