﻿/// @file
/// @brief Contains behavior of interaction with toy car in old room prefab
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Class responsible for interaction action with toy car in old man room
/// </summary>
public class OldManCar : Interactable
{
	/// <summary>
	/// Reference to door game object in the old man room prefab
	/// </summary>
	[SerializeField]
	private GameObject _doorobject = null;

	/// <summary>
	/// Reference to particle system associated with the toy car in old man room
	/// prefab
	/// </summary>
	[SerializeField]
	private GameObject _particleSystem = null;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	private void Awake()
	{
		AddActionsToInteraction(
			EnableDoor,
			DisableParticles,
			ResetLayer,
			RemoveScriptFromObject);
	}

	/// <summary>
	/// Method to enable hidden door within old man room prefab
	/// </summary>
	private void EnableDoor()
	{
		_doorobject.SetActive(true);
	}

	/// <summary>
	/// Method to disable particle system associated with the toy car
	/// </summary>
	private void DisableParticles()
	{
		_particleSystem.SetActive(false);
	}
}
