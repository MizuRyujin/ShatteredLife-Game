﻿/// @file
/// @brief Contains behaviour for the race track interaction and ride
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using System.Collections;
using UnityEngine;

/// <summary>
/// Class responsible for the race track interaction and actual ride
/// </summary>
public class RaceTrackToggle : Interactable
{
    /// <summary>
    /// Variable to be used as value for _timeToRide variable
    /// </summary>
    [SerializeField]
    private float _secToWait = default;

    [SerializeField]
    /// <summary>
    /// Variable to control the time of the ride, different for each race track
    /// </summary>
    private float _secToRide = default;

    /// <summary>
    /// Reference for a cinemachine virtual camera
    /// </summary>
    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera _vcam = null;

    /// <summary>
    /// Reference for a cinemachine dolly cart
    /// </summary>
    [SerializeField]
    private Cinemachine.CinemachineDollyCart _cart = null;

    [SerializeField]
    /// <summary>
    /// Bool to check if raceTrack is the final to advance the puzzle
    /// </summary>
    private bool _isLastRaceTrack = default;

    /// <summary>
    /// Reference to the Action Man game object
    /// </summary>
    private GameObject _actionMan = default;

    /// <summary>
    /// Reference to the Action Man script
    /// </summary>
    private ActionMan _aScript = default;

    /// <summary>
    /// Reference to access CharacterController
    /// </summary>
    private CharacterController _aController = default;

    /// <summary>
    /// Exit position for the Action Man to be used after interaction with final
    /// raceTrack (marked through _isLastRaceTrack)
    /// </summary>
    private Transform _lastTrackExit = default;

    /// <summary>
    /// Variable to be used in RidingTheTrack coroutine
    /// </summary>
    private WaitForSeconds _timeToRide = default;

    /// <summary>
    /// Variable to store information about created dolly track
    /// </summary>
    private Cinemachine.CinemachineTrackedDolly _track = default;

    /// <summary>
    /// Turns true if the player has riden the track
    /// </summary>
    private bool _ride;

    /// <summary>
    /// Instance of AudioCaller for songs and track sound
    /// </summary>
    [SerializeField] private AudioCaller _audioManager;

    /// <summary>
    /// Track sound while riding
    /// </summary>
    [SerializeField] private AudioClip _trackSound;


    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        _actionMan = GameObject.Find("ActionMan");
        _aScript = _actionMan?.GetComponent<ActionMan>();
        _aController = _actionMan?.GetComponent<CharacterController>();
        _timeToRide = new WaitForSeconds(_secToRide);
        _track = _vcam.GetCinemachineComponent<
            Cinemachine.CinemachineTrackedDolly>();
        AddActionsToInteraction(RideRaceTrack);
        _lastTrackExit = GameObject.Find("LastRaceTrackExit").transform;
        _ride = false;
    }

    /// <summary>
    /// Action Man track ride interaction
    /// </summary>
    private void RideRaceTrack()
    {
        // Play track sound
        _audioManager.PlaySound(_trackSound, gameObject);
        _aScript.DisableActionMesh();

        // If it's the first time riding the track
        if (!_ride)
        {
            // Play track music
            _audioManager.PlayMusic(0);
        }

        // If it is the last track, move Action Man to final position
        if (_isLastRaceTrack)
        {
            _aController.enabled = false;
            StartCoroutine(RidingTheTrack());
        }
        else
            StartCoroutine(RidingTheTrack());

        _ride = true;
    }

    /// <summary>
    /// Coroutine to handle the race track ride
    /// </summary>
    /// <returns> _timeToRide </returns>
    private IEnumerator RidingTheTrack()
    {
        _aScript.RaceTrackInteraction();
        yield return StartCoroutine(WaitBeforeTakeOff());

        yield return _timeToRide;

        _aScript.RaceTrackInteraction();
        StartCoroutine(WaitBeforeReset());


        if (_isLastRaceTrack)
        {
            _actionMan.transform.position = _lastTrackExit.position;
        }

        _aScript.EnableActionMesh();
        _aController.enabled = true;

    }

    /// <summary>
    /// Coroutine to stop the race track ride to star before player camera is 
    /// active and in place
    /// </summary>
    /// <returns> _secToWait </returns>
    private IEnumerator WaitBeforeTakeOff()
    {
        _track.m_PathPosition = 0.0f;
        _cart.m_Position = 0.0f;
        yield return _secToWait;
        _vcam.enabled = true;
        _cart.enabled = true;
    }

    /// <summary>
    /// Coroutine to stop the dolly cart and race track vCam reset before player
    /// vCam is active
    /// </summary>
    /// <returns> _secToWait </returns>
    private IEnumerator WaitBeforeReset()
    {
        yield return _secToWait;
        _vcam.enabled = false;
        _cart.enabled = false;
    }
}
