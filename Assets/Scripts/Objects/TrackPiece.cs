﻿/// @file
/// @brief 
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Piece of RaceTrack
/// </summary>
public class TrackPiece : MonoBehaviour
{
    /// <summary>
    /// Auxiliary variable
    /// </summary>
    [SerializeField] private int _index = default;

    /// <summary>
    /// Defines index of for track to know
    /// </summary>
    public int Index => _index;

    /// <summary>
    /// Initial Position
    /// </summary>
    private Vector3 initPosition;

    /// <summary>
    /// Start, to assign position
    /// </summary>
    private void Start()
    {
        // Get initial position
        initPosition = transform.position;
        Physics.IgnoreLayerCollision(12,13);
    }

    /// <summary>
    /// Reset piece position
    /// </summary>
    public void ResetPosition()
    {
        transform.position = initPosition;
    }
}
