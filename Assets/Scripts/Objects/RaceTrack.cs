﻿/// @file
/// @brief 
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// RaceTrack class, manages itself by switching tracks accordingly
/// </summary>
public class RaceTrack : MonoBehaviour
{
    /// <summary>
    /// Array that contains the tracks to be used
    /// </summary>
    [SerializeField] private GameObject[] trackMeshes = null;

    /// <summary>
    /// Array of audioclips containing the piece sounds
    /// </summary>
    [SerializeField] private AudioClip[] _pieceAudio;

    /// <summary>
    /// Instance of AudioCaller - Audio Manager
    /// </summary>
	[SerializeField] private AudioCaller _audioManager;

    /// <summary>
    /// Reference to the last track shown
    /// </summary>
    private GameObject _lastTrack;

    /// <summary>
    /// Start, instantiates first mesh in track
    /// </summary>
    public void Start()
    {
        // Instantiate starter mesh
        _lastTrack = Instantiate(trackMeshes[0], transform);
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// Detects collision and switches tracks accordingly
    /// </summary>
    /// <param name="col"> The other Collider involved in this collision. </param>
    public void OnTriggerEnter(Collider col)
    {
        TrackPiece trackP = col.gameObject.GetComponent<TrackPiece>();

        if (trackP)
        {
            // Hide piece and set default position
            trackP.ResetPosition();

            // Remove mesh
            Destroy(_lastTrack);

            // Add/change mesh
            _lastTrack = Instantiate(trackMeshes[trackP.Index], transform);

            // Play sound
            _audioManager.PlaySound(_pieceAudio[trackP.Index], gameObject);
        }
    }
}
