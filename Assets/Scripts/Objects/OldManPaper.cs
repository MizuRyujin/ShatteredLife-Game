﻿/// @file
/// @brief Contains behavior of interaction with hidden paper in old room prefab
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Class responsible for hidden paper behaviour within the old man room
/// </summary>
public class OldManPaper : Interactable
{
    /// <summary>
    /// Reference to a game object associated with the memory puzzle
    /// section player will unlock
    /// </summary>
    [SerializeField]
    private GameObject _memoryTrigger = null;

    /// <summary>
    /// Reference to particle system associated with the toy car in old man room
	/// prefab
    /// </summary>
    [SerializeField]
    private GameObject _particleSystem = null;

    /// <summary>
    /// Reference to the audio to be played after interacting with the paper
    /// </summary>
	[SerializeField] private InteractionNarration _narrationScript = null;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        AddActionsToInteraction(
            EnableMemoryKey,
            DisableParticles,
            ResetLayer,
            RemoveScriptFromObject,
            PlayNarration);
    }

    /// <summary>
    /// Method to enable the game object referenced as the memory trigger
    /// </summary>
    private void EnableMemoryKey()
    {
        _memoryTrigger.SetActive(true);
    }

    /// <summary>
    /// Method to disable particle system associated with the toy car
    /// </summary>
    private void DisableParticles()
    {
        _particleSystem.SetActive(false);
    }

    /// <summary>
    /// Method to play the narration audio clip associated with the mug
    /// </summary>
    private void PlayNarration()
    {
        _narrationScript.PlayClip();
    }
}
