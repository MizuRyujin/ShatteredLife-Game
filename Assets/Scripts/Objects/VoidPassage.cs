﻿/// @file
/// @brief Contains behavior for Void Passage prefab, to teleport the player to
/// the Void Room prefab existing in the same scene
/// 
/// @author Miguel Fernández, João Rebelo, Rodrigo Pinheiro, Tommás Franco.
/// @date 2020

using System.Collections;
using UnityEngine;

/// <summary>
/// Class responsible for the void room teleportation behaviour
/// </summary>
public class VoidPassage : MonoBehaviour
{
    /// <summary>
    /// Editor variable to define seconds value to be used in WaitForSeconds
    /// variable
    /// </summary>
    [SerializeField]
    private float _secToWait = default;

    /// <summary>
    /// Editor variable to reference void room spawn point
    /// </summary>
    [SerializeField]
    private Transform _voidSpawn = default;

    /// <summary>
    /// Position to return Action Man to original spot before usage
    /// </summary>
    [SerializeField]
    private Transform _actOriginalSpawn = default;

    /// <summary>
    /// Reference to Player object
    /// </summary>
    private GameObject _player = default;

    /// <summary>
    /// Reference to the PauseEffect script
    /// </summary>
    private PauseEffect _playerPause = default;

    /// <summary>
    /// Reference to the player script in scene.
    /// </summary>
    private Player _pScript = default;

    /// <summary>
    /// Reference to Action Man object
    /// </summary>
    private GameObject _actionMan = default;

    /// <summary>
    /// Reference to Action Man script
    /// </summary>
    private ActionMan _actScript = default;

    /// <summary>
    /// Reference to Player's CharacterController
    /// </summary>
    private CharacterController _pCC = default;

    /// <summary>
    /// Reference to Action Man's CharacterController
    /// </summary>
    private CharacterController _aCC = default;

    /// <summary>
    /// Time value (in seconds) to be used in a coroutine
    /// </summary>
    private WaitForSeconds _seconds = default;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        _seconds = new WaitForSeconds(_secToWait);
        _player = GameObject.Find("Player");
        _pScript = _player.GetComponent<Player>();
        _actionMan = GameObject.Find("ActionMan");
        _actScript = _actionMan.GetComponent<ActionMan>();
        _pCC = _player.GetComponent<CharacterController>();
        _playerPause = _player.GetComponentInChildren<PauseEffect>();
        _aCC = _actionMan.GetComponent<CharacterController>();
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            TeleportPlayer();
        }
    }

    /// <summary>
    /// Method to be used in a event Action and teleport player
    /// to some position
    /// </summary>
    private void TeleportPlayer()
    {
        _playerPause.CloseEyes();
        StartCoroutine(WaitBeforeTeleport());
    }

    /// <summary>
    /// Coroutine to have an animation play before teleportation of player
    /// </summary>
    /// <returns> Time to wait before teleport </returns>
    private IEnumerator WaitBeforeTeleport()
    {
        _pCC.enabled = false;
        _aCC.enabled = false;
        yield return _seconds;
        _player.transform.position = _voidSpawn.position;
        _actionMan.transform.position = _actOriginalSpawn.position;
        _pScript.ActionManInteraction();
        _actScript._canMove = false;
        _actScript._cameraActive = false;
        _actScript.DisableVcam();
        _pCC.enabled = true;
        _aCC.enabled = true;
    }
}
