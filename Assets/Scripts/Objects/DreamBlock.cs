﻿/// @file
/// @brief 
/// 
/// @author Miguel Fernández, João Rebelo, Tomás Franco, Rodrigo Pinheiro
/// @date 2020
using UnityEngine;

/// <summary>
/// Block that collides with marble and floats
/// </summary>
public class DreamBlock : MonoBehaviour
{
    /// <summary>
    /// Block's rigidbody
    /// </summary>
    private Rigidbody _rb;

    /// <summary>
    /// GameObject to colide with
    /// </summary>
    private GameObject _object;

    /// <summary>
    /// Impact sound to be played
    /// </summary>
    [SerializeField] private AudioClip _blockSound;

    /// <summary>
    /// Instance of AudioCaller - Audio Manager
    /// </summary>
	[SerializeField] private AudioCaller _audioManager;

    /// <summary>
    /// Start used to assign variables
    /// </summary>
    private void Start()
    {
        _object = GameObject.FindWithTag("Marble");
        _rb = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Detect colision and makes object float
    /// </summary>
    /// <param name="col"> Accepts a collision </param>
    public void OnCollisionEnter(Collision col)
    {
        Vector3 forceDir;
        Vector3 torque;
        float upwardForce = 0.002f;

        // When the marble colides with the blocks
        if (col.gameObject == _object)
        {
            // Play sound
            _audioManager?.PlaySound(_blockSound, gameObject, Random.Range(0.95f, 1f));

            forceDir = col.impulse;
            torque = col.impulse;

            // Add flying effect math
            forceDir.y += upwardForce;
            torque.z += 0.3f;

            // Add flying effect methods
            _rb.AddForce(forceDir);
            _rb.AddTorque(torque);
            _rb.useGravity = false;
        }

    }
}
